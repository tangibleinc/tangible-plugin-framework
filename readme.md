
# Tangible Plugin Framework module

Reference: https://docs.tangible.one/modules/plugin-framework

## Use

Use the function `tangible`, inside or after the `plugins_loaded` action hook, to get the latest version of the framework.

```php
require_once __DIR__.'/vendor/tangible/plugin-framework/index.php';

add_action('plugins_loaded', function() {

  $framework = tangible();

  $framework->register_plugin([
    // ..Plugin config..
  ]);

});
```
