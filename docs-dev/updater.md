
## Note on plugin updater feature

There is a Git branch called `variant/without-updater`, which removed the EDD updater class to comply with WordPress plugin directory's rules.

This branch should merge from `master` regularly, to provide new updates to Tangible plugins that are published there.
