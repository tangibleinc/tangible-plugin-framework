<?php

require_once __DIR__.'/action-api/index.php';
require_once __DIR__.'/ajax/index.php';
require_once __DIR__.'/async-action/index.php';
require_once __DIR__.'/background-queue/index.php';
require_once __DIR__.'/date/index.php';
require_once __DIR__.'/duplicate-post/index.php';
require_once __DIR__.'/filesystem/index.php';
require_once __DIR__.'/hjson/index.php';
require_once __DIR__.'/html/index.php';
require_once __DIR__.'/preact/index.php';
require_once __DIR__.'/rest-api/index.php';
require_once __DIR__.'/sortable-post-type/index.php';
require_once __DIR__.'/tester/index.php';

// Depends on async action and Action API
require_once __DIR__.'/analytics/index.php';
