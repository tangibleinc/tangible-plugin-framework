<?php

// Actions

$action_api->action_types = [
  'action' => [] // name => callback
];

// Add

$action_api->add_action = function($action, $callback) use ($action_api) {
  return $action_api->add_type_action('action', $action, $callback);
};

$action_api->add_type_action = function($type, $action, $callback) use ($action_api) {
  $action_api->action_types[ $type ][ $action ] = $callback;
};


// Run

$action_api->run_action = function($action, $request = []) use ($action_api) {
  return $action_api->run_type_action('action', $action, $request);
};

$action_api->run_type_action = function($type, $action, $request = []) use ($action_api) {

  if (!isset($action_api->action_types[ $type ])) {

    $action_api->status = 404;

    return [
      'message' => 'Type ' . $type . ' not found',
    ];
  }

  if (!isset($action_api->action_types[ $type ][ $action ])) {

    $action_api->status = 404;

    return [
      'message' => 'Action "' . $action . '" not found'
        .( $type==='action' ? '' : ' for type "' . $type . '"' ),
    ];
  }

  /**
   * Action result
   *
   * If run_type_action() is called directly, caller should check if ($action_api->is_success())
   * for success, otherwise handle error.
   *
   * For action API request, see ./handler.php where response status header is set.
   */

  $action_api->status = 200;

  return call_user_func(
    $action_api->action_types[ $type ][ $action ],
    $request
  );
};


/**
 * Status for HTTP response code
 */

$action_api->status = 200;

$action_api->set_status = function($status = []) use ($action_api) {
  $action_api->status = $status;
};

$action_api->is_success = function() use ($action_api) {
  return $action_api->status === 200;
};
