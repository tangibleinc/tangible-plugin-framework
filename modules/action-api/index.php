<?php
/**
 * General-purpose Action API at POST /tangible-action-api
 *
 * Similar to AJAX module interface: simpler and more flexible than REST API,
 * and closer to RPC (remote procedure call).
 */

$framework->action_api = function() use ($framework) {

  static $action_api;

  if ($action_api) return $action_api;

  $action_api = $framework->object([
    'name' => 'tangible_action_api',
  ]);

  include __DIR__.'/actions.php';
  include __DIR__.'/handler.php';

  return $action_api;
};

// Global shortcut
if ( ! function_exists( 'tangible_action_api' ) ) :
function tangible_action_api() {
  return tangible()->action_api();
}
endif;
