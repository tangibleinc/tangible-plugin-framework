<?php
/**
 * Route handler
 */

add_action('template_redirect', function($template) use ($framework, $action_api) {

  // Allowed methods

  if ( ! in_array($_SERVER['REQUEST_METHOD'], ['POST', 'OPTIONS'])) return $template;

  // Match route

	global $wp;

  $route_parts = explode('/', $wp->request);

  if (!isset($route_parts[0]) || $route_parts[0] !== 'tangible-action-api') return $template;

  array_shift( $route_parts );


  // Response header

  header("Access-Control-Allow-Origin: *");
  header("Access-Control-Allow-Methods: POST, OPTIONS");

  if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    // Handle preflight OPTIONS request

    status_header(200);

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
      header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    }

    exit;
  }

  header('Content-Type: application/json');


  // Process request body

  $request = file_get_contents("php://input");

  if (empty($request)) {

    $request = [];

  } else {

    // Must be valid JSON

    try {
      $request = json_decode($request, true);
    } catch (\Throwable $th) {
      status_header(400); // Bad request
      exit;
    }
  }


  // Get type, action, data

  $parts_count = count( $route_parts );

  if ($parts_count===0) {

    // Route "/"

    $type   = isset($request['type']) ? $request['type'] : 'action';
    $action = isset($request['action']) ? $request['action'] : '';
    $data   = isset($request['data']) ? $request['data'] : [];

  } elseif ($parts_count===1) {

    // Route "/action"

    $type   = 'action';
    $action = $route_parts[0];
    $data   = $request;

  } elseif ($parts_count===2) {

    // Route "/type/action"

    $type   = $route_parts[0];
    $action = $route_parts[1];
    $data   = $request;
  }


  // Run action

  $response = $action_api->run_type_action( $type, $action, $data );


  status_header( $action_api->status );

  echo json_encode( $response );

  exit;

}, 1);
