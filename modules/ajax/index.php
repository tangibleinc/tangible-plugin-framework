<?php
/**
 * AJAX
 *
 * Example:
 *
 * ```php
 * $framework->ajax()->enqueue();
 *
 * $framework->ajax()->add_action('test_action', function($data, $ajax) {
 *   if (!isset($data['from'])) {
 *     return $ajax->errror('Invalid request');
 *   }
 *   return [ 'hello' => $data['from'] ];
 * });
 * ```
 *
 * ```js
 * const { ajax } = window.Tangible
 *
 * ajax('test_action', { from: 'client' })
 *   .then(result => {})
 *   .catch(error => {})
 * ```
 */

$framework->ajax = function() use ($framework) {

  static $ajax;

  if ($ajax) return $ajax;

  $ajax = $framework->object([
    'enqueued' => false,
  ]);

  include __DIR__.'/actions.php';
  include __DIR__.'/enqueue.php';
  include __DIR__.'/nonce.php';

  return $ajax;
};

// Global shortcut
if ( ! function_exists( 'tangible_ajax' ) ) :
function tangible_ajax() {
  return tangible()->ajax();
}
endif;
