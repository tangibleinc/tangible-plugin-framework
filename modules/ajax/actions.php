<?php

// Actions

$ajax->error = function($data = []) {
  wp_send_json_error(
    is_a($data, 'Exception')
      ? $data->getMessage()
      : (is_string($data) ? [ 'message' => $data ] : $data)
  );
  exit;
};

$ajax->success = function($data = []) {
  wp_send_json_success($data);
  exit;
};

$ajax->add_action = function($name, $fn, $options = []) use ($ajax) {

  if (!isset($options['public']) || !$options['public']) {

    // Logged-in users only by default
    $action_name = "wp_ajax_tangible_ajax_{$name}";

  } else {

    // Public, not logged-in
    $action_name = "wp_ajax_nopriv_tangible_ajax_{$name}";

    // Register for logged-in users also
    unset($options['public']);
    $ajax->add_action($name, $fn, $options);
  }

  add_action($action_name, function() use ($ajax, $fn) {

    if (!$ajax->verify_nonce()) {
      return $ajax->error('Bad nonce');
    }

    $data = isset($_POST['data']) ? stripslashes_deep($_POST['data']) : [];

    try {
      return $ajax->success($fn($data, $ajax));
    } catch (\Throwable $err) {
      return $ajax->error($err);
    }
  });

  return $ajax;
};

$ajax->add_public_action = function($name, $fn, $options = []) use ($ajax) {
  return $ajax->add_action($name, $fn, [ 'public' => true ] + $options);
};
