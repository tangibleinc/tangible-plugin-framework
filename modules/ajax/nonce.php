<?php

// Nonce

$ajax->nonce_name = 'tangible_ajax';

$ajax->create_nonce = function() use ($ajax) {
  return wp_create_nonce($ajax->nonce_name);
};

$ajax->verify_nonce = function() use ($ajax) {
  return isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], $ajax->nonce_name);
};
