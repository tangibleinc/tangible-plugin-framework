<?php

// Enqueue

$ajax->schedule_enqueue = false;
$ajax->enqueued = false;
$ajax->registered = false;

$ajax->enqueue = function() use ($ajax) {
  if ($ajax->enqueued || $ajax->schedule_enqueue) return;
  $ajax->schedule_enqueue = true;
};

$ajax->register_library = function() use ($framework, $ajax) {

  if (!$framework->is_latest_version() || $ajax->registered) return;

  wp_deregister_script('tangible-ajax'); // Override previous versions

  wp_register_script('tangible-ajax',
    "{$framework->url}/assets/build/ajax.min.js",
    ['jquery'], $framework->version, true
  );

  wp_localize_script('tangible-ajax', 'TangibleAjaxConfig', [
    'url' => admin_url('admin-ajax.php'),
    'nonce' => $ajax->create_nonce()
  ]);

  $ajax->registered = true;
};

$ajax->conditional_enqueue_library = function() use ($ajax) {

  if (!$ajax->schedule_enqueue || $ajax->enqueued) return;
  if (!$ajax->registered) {
    $ajax->register_library();
  }

  wp_enqueue_script('tangible-ajax');

  $ajax->schedule_enqueue = false;
  $ajax->enqueued = true; // Run only once
};

add_action('wp_enqueue_scripts', $ajax->register_library, 1);
add_action('admin_enqueue_scripts', $ajax->register_library, 1);

add_action('wp_enqueue_scripts', $ajax->conditional_enqueue_library, 999);
add_action('admin_enqueue_scripts', $ajax->conditional_enqueue_library, 999);

add_action('wp_footer', $ajax->conditional_enqueue_library, 0);
add_action('admin_footer', $ajax->conditional_enqueue_library, 0);
