<?php

$rest_api->request = function($method, $route, $params = [], $options = []) {

  $method = strtoupper($method);
  $request = new WP_REST_Request( $method, $route );

  if ($method==='GET') {
    $request->set_query_params( $params );
  } else {
    $request->set_body_params( $params );
  }

  $response = rest_do_request( $request );

  if ( $response->is_error() ) {

    /**
     * Return WP_Error object
     * Caller should check is_wp_error( $result )
     */
     return $response->as_error();
  }

  // $response->get_data()

  /**
   * Embed option - If true, fields like authors and featured images are embedded.
   *
   * @see https://developer.wordpress.org/reference/classes/wp_rest_server/response_to_data/
   */
  $embed = isset($options['embed']) ? $options['embed'] : false;

  return rest_get_server()->response_to_data(
    $response,
    $embed
  );
};

foreach ([
  'get',
  'post',
  'put',
  'patch',
  'delete'
] as $key) {
  $rest_api->$key = function() use ($key, $rest_api) {
    $args = func_get_args();
    array_unshift($args, $key);
    return call_user_func_array($rest_api->request, $args);
  };
}
