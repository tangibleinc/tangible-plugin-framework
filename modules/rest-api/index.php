<?php

/**
 * REST API module
 *
 * Wrapper to use WordPress REST API internally without any HTTP requests.
 *
 * Based on idea from: [Making WordPress REST API Calls Internally](https://wpscholar.com/blog/internal-wp-rest-api-calls/)
 */

$framework->rest_api = function() use ($framework) {
  static $rest_api;
  if (!$rest_api) {

    $rest_api = tangible_object([
      'name' => 'tangible_rest_api',
    ]);

    require_once __DIR__.'/methods.php';
  }
  return $rest_api;
};

// Global shortcut
if ( ! function_exists( 'tangible_rest_api' ) ) :
function tangible_rest_api() {
  return tangible()->rest_api();
}
endif;
