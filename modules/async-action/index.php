<?php

$framework->register_async_action = function($config) use ($framework) {

  static $register;

  if (!$register) {
    $register = require_once __DIR__.'/register.php';
  }

  return $register( $config );
};

// Global shortcut
if ( ! function_exists( 'tangible_async_action' ) ) :
function tangible_async_action($config) {
  return tangible()->register_async_action($config);
}
endif;
