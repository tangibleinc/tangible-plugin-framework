<?php
/**
 * Sortable post type
 *
 * Adds support for drag-and-drop sort in any post type, corresponding to field "menu_order"
 *
 * Adapted from: https://github.com/ColorlibHQ/simple-custom-post-order
 */

$framework->register_sortable_post_type = function( $type ) use ($framework) {

  static $sortable_post_type = null;

  if ( ! $sortable_post_type ) {

    require_once __DIR__ . '/class-sortable-post-type.php';

    Tangible\PluginFramework\SortablePostType::$framework = $framework;

    $sortable_post_type = new Tangible\PluginFramework\SortablePostType;
  }

  $sortable_post_type->register( $type );
};
