<?php

/**
 * HTML module
 *
 * Implements an HTML processor to provide dynamic template tags.
 *
 * Included in the framework (moved from Template module), since features are also
 * used by the Loop module, for example, to build image tag with attributes.
 */

if ( ! function_exists( 'tangible_html' ) ) :
function tangible_html() {
  static $html;
  return $html ? $html : (
    $html = tangible_object([
      'name' => 'html'
    ])
  );
}
endif;

$html = tangible_html();

require_once __DIR__.'/tag/index.php';
require_once __DIR__.'/parse/index.php';
require_once __DIR__.'/render/index.php';

require_once __DIR__.'/tags/index.php';
require_once __DIR__.'/utils/index.php';

require_once __DIR__.'/filters/index.php';
