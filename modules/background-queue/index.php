<?php

/**
 * Register background queue
 *
 * Queue must be registered to process the tasks
 *
 * @param object    config
 * @param string    config.name       Name of queue
 * @param function  config.callback   Task callback for each item
 * @param function  config.complete   Complete callback when queue is done
 * @param function  config.error      Error callback when a task callback failed
 *
 * @return $queue   Call $queue->add_items([ ... ]) to push items to queue
 */
$framework->register_background_queue = function($config) use ($framework) {

  if (!class_exists('Tangible\\BackgroundQueue')) {
    require_once __DIR__.'/class-background-queue.php';
  }

  return new Tangible\BackgroundQueue( $config );
};

// Global shortcut
if ( ! function_exists( 'tangible_background_queue' ) ) :
function tangible_background_queue($config) {
  return tangible()->register_background_queue($config);
}
endif;
