<?php
/**
 * Site statistics
 */

$framework->get_site_stats = function() use ($framework) {

  global $wp_version;

  $stats = [
    // Common to plugin stats
    'siteUrl' => site_url(),

    'wordpressVersion' => $wp_version,
    'phpVersion' => phpversion(),
    'pluginFrameworkVersion' => $framework->version,
    'pluginFrameworkDir' => plugin_basename(plugin_dir_path($framework->file_path)),
  ];

  /**
   * Theme
   */

  $theme = wp_get_theme();

  $stats['themeName'] = $theme->stylesheet;
  $stats['themeTitle'] = $theme->display('Name');
  $stats['themeVersion'] = $theme->display('Version');

  $parent_theme = $theme->parent();
  if (!empty($parent_theme)) {
    $stats['parentThemeName'] = $parent_theme->template;
    $stats['parentThemeTitle'] = $parent_theme->display('Name');
    $stats['parentThemeVersion'] = $parent_theme->display('Version');
  }


  /**
   * User counts
   * @see https://developer.wordpress.org/reference/functions/count_users/
   */

  $user_count_data = count_users();

  $stats['userCount'] = (int) $user_count_data['total_users'];
  $stats['userCountPerRole'] = $user_count_data['avail_roles'];



  /**
   * Post counts
   * @see https://developer.wordpress.org/reference/functions/get_post_types/
   * @see https://developer.wordpress.org/reference/functions/wp_count_posts/
   */

  $post_count_per_post_type = [];

  foreach (get_post_types([
    // 'public'   => true,
    '_builtin' => true,
  ]) as $name) {

    $count = wp_count_posts( $name );

    $post_count_per_post_type[ $name ] = !empty($count) && isset($count->publish)
      ? (int) $count->publish
      : 0
    ;
  }

  $stats['postCountPerPostType'] = $post_count_per_post_type;


  /**
   * Plugins - Version, active status
   */

  if( ! function_exists( 'get_plugins' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
  }

  $plugins = [];

  foreach (get_plugins() as $key => $data) {

    // Key is plugin entry file path, like hello-dolly/hello.php
    $name = dirname($key);

    $plugins []= [
      'name'     => $name,
      'title'    => $data['Title'],
      'version'  => $data['Version'],
      'active'   => is_plugin_active( $key ),
      // 'entry_file_path' => $key,
    ];
  }

  $stats['installedPlugins'] = $plugins;


  return $stats;
};
