<?php

$framework->analytics_cronjob_name = $framework->name . '_analytics_crojob_with_user_content';

add_action($framework->analytics_cronjob_name, function() use ($framework) {

  if ( ! $framework->is_site_analytics_enabled() ) return;

  $framework->log_to_analytics(
    'siteStats',                  // Type
    'Site statistics',            // Message
    $framework->get_site_stats()  // Data
  );

});

/**
 * Ensure schedule "weekly" is defined
 */
add_filter('cron_schedules', function($schedules) {

  if (!isset($schedules['weekly'])) {
    $schedules['weekly'] = [
      'interval' => 7 * 24 * 60 * 60, //7 days * 24 hours * 60 minutes * 60 seconds
      'display' => __( 'Once Weekly', 'my-plugin-domain' )
    ];
  }

  return $schedules;

}, 99, 1);

if ( ! wp_next_scheduled( $framework->analytics_cronjob_name ) ) {
  wp_schedule_event( time(), 'weekly', $framework->analytics_cronjob_name );
}
