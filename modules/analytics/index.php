<?php
/**
 * Log data for anonymous usage analytics, with user consent
 */

$framework->analytics_url = 'https://analytics.tangible.one/log';

/**
 * Check if specific plugin has analytics enabled
 */
$framework->is_plugin_analytics_enabled = function( $plugin_name ) use ($framework) {
  $plugin = $framework->get_api( $plugin_name );
  return !empty( $plugin ) && $plugin->analytics_log_enabled();
};


/**
 * Check if any plugin has analytics enabled
 */
$framework->is_site_analytics_enabled = function() use ($framework) {

  foreach ($framework->get_plugins() as $name => $plugin) {
    if ($plugin->analytics_log_enabled()) return true;
  }

  return false;
};

require_once __DIR__.'/cron.php';
require_once __DIR__.'/log.php';
require_once __DIR__.'/ping.php';
require_once __DIR__.'/stats.php';
