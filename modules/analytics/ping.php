<?php
/**
 * Minimal "ping" API endpoint for site live status check: POST /tangible-action-api/analytics/ping
 */

$framework->action_api()->add_type_action('analytics', 'ping', function() {
  return [
    'status' => 'OK'
  ];
});
