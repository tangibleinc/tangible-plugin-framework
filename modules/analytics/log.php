<?php

$framework->log_to_analytics = function( $type, $message = '', $data = [] ) {

  // Ensure latest version
  $framework = tangible();

  if (!isset($framework->async_log_to_analytics)) return;

  // Format data with log type and message

  if (is_array($type)) {
    $data = $type;
  } else {
    $data = array_merge(
      [
        'type' => $type,
        'message' => $message,
      ],
      $data
    );
  }

  // Schedule async action

  return $framework->async_log_to_analytics( $data );
};

/**
 * Async action that actually does the logging. This runs in its own background process.
 */
$framework->async_log_to_analytics = $framework->register_async_action([

  'name' => 'tangible_analytics',

  'action' => function( $data ) use ( $framework ) {

    /**
     * For debugging: Append data to wp-content/log.txt
     */
    // return tangible()->log( 'Log to analytics', json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT) );

    /**
     * Send to analytics endpoint
     *
     * @see https://developer.wordpress.org/reference/functions/wp_remote_post/
     */

    $response = wp_remote_post(
      $framework->analytics_url,
      [
        'method'  => 'POST',
        'body'    => json_encode($data),
        'headers' => array(
          'Content-Type' => 'application/json'
        ),
        'sslverify' => false, // TODO: Fix this on analytics domain?
      ]
    );

    if ( is_wp_error( $response ) ) {
      // $response->get_error_message()
    }
  },
]);
