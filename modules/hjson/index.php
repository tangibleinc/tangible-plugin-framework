<?php

// Based on HJSON-PHP v2.1.0 - https://github.com/hjson/hjson-php

$framework->hjson = function($content = false) use ($framework) {

  static $hjson;

  if ( $hjson ) return $content===false ? $hjson : $hjson->parse($content);

  require_once __DIR__.'/vendor/HJSONException.php';
  require_once __DIR__.'/vendor/HJSONUtils.php';

  $hjson = tangible_object([
    'name' => 'hjson',
    'parser' => null,
    'stringifier' => null,
  ]);

  $hjson->parse = function($source, $options = []) use ($hjson) {

    // Load on demand
    if (empty($hjson->parser)) {
      require_once __DIR__.'/vendor/HJSONParser.php';
      $hjson->parser = new Tangible\Module\HJSON\HJSONParser();
    }

    $hjson_options = [
      'assoc' => true // Return associative array instead of object
    ];

    if (isset($options['throw'])) {
      return $hjson->parser->parse( $source, $hjson_options );
    }

    // Silent on error
    try {
      return $hjson->parser->parse( $source, $hjson_options );
    } catch (Exception $e) {
      return [];
    }
  };

  $hjson->render = function($source, $options = []) use ($framework, $hjson) {

    // Load on demand
    if (empty($hjson->stringifier)) {
      require_once __DIR__.'/vendor/HJSONStringifier.php';
      $hjson->stringifier = new Tangible\Module\HJSON\HJSONStringifier();
    }

    // Turn associative array into object
    if ($framework->is_associative_array( $source ) || isset($options['object'])) {
      $source = (object) $source;
    }

    if (isset($options['throw'])) {
      return $hjson->stringifier->stringify( $source );
    }

    // Silent on error
    try {
      return $hjson->stringifier->stringify( $source );
    } catch (Exception $e) {
      return [];
    }
  };

  return $hjson;
};

// Global shortcut
if ( ! function_exists( 'tangible_hjson' ) ) :
function tangible_hjson( $content = false ) {
  return tangible()->hjson(  $content );
}
endif;
