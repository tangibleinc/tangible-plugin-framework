<?php

// global $framework, $test

$test('Preact', function($it) use ($framework) {

  $framework->enqueue_preact();

  $it('enqueue', true);

  ?>
<div id="preact-test"></div>
<script>
jQuery(function($) {

  const Preact = window.Tangible.Preact

  console.log('window.Tangible.Preact', Preact)

  const h = Preact.createElement
  const $preact = document.getElementById('preact-test')

  Preact.render(
    h('span', null, [
      'Hello from Preact'
    ]),
  $preact)
})
</script>
  <?php

});
