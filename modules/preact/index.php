<?php


$framework->preact_registered = false;

// Register script and style

$framework->register_preact = function() use ($framework) {

  wp_register_script('tangible-preact',
    "{$framework->url}/assets/build/preact.min.js",
    [],
    '10.4.4',
    true
  );

  $framework->preact_registered = true;
};

add_action('wp_enqueue_scripts', $framework->register_preact, 0);
add_action('admin_enqueue_scripts', $framework->register_preact, 0);

// Enqueue

$framework->enqueue_preact = function($options = []) use ($framework) {

  $enqueue = function() use ($framework, $options) {
    wp_enqueue_script('tangible-preact');
  };

  // During or after wp_enqueue_scripts
  if ($framework->preact_registered) return $enqueue();

  // If called before register
  if ( is_admin() ) {
    add_action('admin_enqueue_scripts', $enqueue);
  } else {
    add_action('wp_enqueue_scripts', $enqueue);
  }
};
