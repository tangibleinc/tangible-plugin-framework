<?php

$framework->tester = function() use ($framework) {
  static $tester;
  if (!$tester) {

    $tester = tangible_object([
      'name' => 'tangible_tester',
      'version' => '20200721',
    ]);

    require_once __DIR__.'/enqueue.php';
    require_once __DIR__.'/report.php';
    require_once __DIR__.'/test.php';

  }
  return $tester;
};

// Global shortcut
if ( ! function_exists( 'tangible_tester' ) ) :
function tangible_tester() {
  return tangible()->tester();
}
endif;
