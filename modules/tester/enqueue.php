<?php

// Register script and style

$tester->registered = false;

add_action('wp_enqueue_scripts', function() use ($framework, $tester) {

  wp_register_script('tangible-tester',
    "{$framework->url}/assets/build/tester.min.js",
    [],
    $framework->version,
    true
  );

  $tester->registered = true;
}, 0);

// Enqueue

$tester->enqueue = function($options = []) use ($tester) {

  $enqueue = function() use ($options) {
    wp_enqueue_script('tangible-tester');
  };

  // During or after wp_enqueue_scripts
  if ($tester->registered) return $enqueue();

  // If called before register
  add_action('wp_enqueue_scripts', $enqueue);
};
