<?php
/**
 * Create and validate schema
 *
 * The Tangible Builder can generate JSON schema based on TypeScript definitions.
 *
 * This module applies and validates the schema for PHP objects. It's meant to be
 * minimal and practical, and does not support the full JSON Schema specification.
 */

// object, string, array, integer, number, boolean, null, any
