<?php

$framework->register_action_links = function($plugin) use ($framework) {

  // Add "Settings" link in admin plugin list
  // https://codex.wordpress.org/Plugin_API/Filter_Reference/plugin_action_links_(plugin_file_name)

  $basename = plugin_basename($plugin['file_path']);

  $filter = $framework->is_multisite($plugin)
    ? "network_admin_plugin_action_links_$basename"
    : "plugin_action_links_$basename";

  add_filter($filter, function($links) use ($framework, $plugin) {

    $url = $framework->get_settings_page_url($plugin);
    $label = 'Settings';

    $links []= '<a href="'.$url.'">'.$label.'</a>';

    // Additional links, like Support

    if (isset($plugin['action_links'])) {
      foreach ($plugin['action_links'] as $link) {
        $links []= $link;
      }
    }

    return $links;
  }, 10, 1);

};
