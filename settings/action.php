<?php

/**
 * Handle plugin settings action on form submit
 * Called from ../init.php
 */
$framework->settings_action = function($config) use ($framework) {

  $settings_key = $framework->get_settings_key($config);

  $api = $framework->get_api($config);

  $prefix = $config['setting_prefix'];
  $nonce_key = "{$prefix}_nonce";

  if (empty($_POST)
    || !isset($_POST[$settings_key])
    || !check_admin_referer($nonce_key, $nonce_key)
  ) return;

  $settings = $_POST[$settings_key];

  $framework->update_settings($config, $settings);
};
