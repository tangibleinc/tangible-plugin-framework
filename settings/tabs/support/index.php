<?php

$framework->render_support_tab = function($config) use ($framework) {

  $settings_key = $framework->get_settings_key($config);
  $settings = $framework->get_settings($config);

  $api = $framework->get_api($config);

  $log_file_path =
    "plugins/{$config['name']}/{$config['name']}.log";
    //str_replace(WP_CONTENT_DIR, '', $config['log_file_path']);

  $support_email = $framework->support_email;
  $support_email_title = "{$config['title']} issue";
  $support_email_link = "mailto:$support_email?subject=$support_email_title";

  //$framework->see('Settings', $settings);

  ?>
  <div class="tangible-plugin-settings-tab-support">

    <h3>Support</h3>

    <p>Write us at <a href="<?php echo esc_url($support_email_link); ?>"><?php echo $support_email; ?></a> to report an issue.</p>

    <p>Please include a description of how to reproduce the issue, and the setup info below.</p>

    <pre><code><?php echo $api->debug_stats(); ?></code></pre>

    <p>It would be helpful if you could turn on debug logging, and send us the log file (if any).</p>

    <div class="setting-row">
      <?php
      $framework->render_setting_field([
        'type' => 'checkbox',
        'name' => "{$settings_key}[debug_log]",
        'value' => isset($settings['debug_log']) ? $settings['debug_log'] : '',
        'label' => "Enable debug log - saved in <code>$log_file_path</code>",
      ]);

      ?>
      <p style="margin: .5rem 0 0 1.5rem">For sites with a large amount of content, there may be performance impact of recording logs. It's recommended to enable this only when needed, and keep it disabled for production sites.</p>
      <?php

      /**
       * If log file exists, provide and handle remove action
       */

      if (file_exists($config['log_file_path'])) {
        ?><div style="margin: 1rem 0 0 0"><?php
        if (!empty($_POST['plugin_framework_remove_log_file'])) {
          echo unlink($config['log_file_path'])
            ? 'Log file removed.'
            : 'Failed to remove log file. Please do it manually.'
          ;
        } else {
          ?>
            <span id="plugin-framework-remove-log-file">
              <input type="hidden" name="plugin_framework_remove_log_file" value="">
              <button type="submit" class="button">Remove log file</button>
            </span>
            <script>
            jQuery(function($) {
              var $action = $('#plugin-framework-remove-log-file')
              var $button = $action.find('button')
              var $input  = $action.find('input')
              $button.on('click', function() {
                $input.val(1)
              })
            })
            </script>
          <?php
        }
        ?></div><?php
      }
      ?>
    </div>
    <div class="setting-row">
      <?php
      $framework->render_setting_field([
        'type' => 'checkbox',
        'name' => "{$settings_key}[analytics_log]",
        'value' => isset($settings['analytics_log']) ? $settings['analytics_log'] : '',
        'label' => "Enable anonymous usage analytics to help us improve your experience.",
      ]);
      ?>
    </div>
    <?php submit_button(); ?>
  </div>
  <?php
};
