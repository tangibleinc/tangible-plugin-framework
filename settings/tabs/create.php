<?php

// Called on current_screen hook from ../plugin/api

$framework->create_plugin_settings_tabs = function($config) use ($framework) {

  $tabs = [];

  // Default setting tabs: Features, Recommend, License, Support

  if (isset($config['feature_groups'])) {
    foreach ($config['feature_groups'] as $features_config) {
      // Render feature group tab
      $prefix = $features_config['setting_prefix']; // "features" by default
      $tabs[$prefix] = $features_config+[
        'callback' => function() use ($framework, $config, $features_config) {
          // @see ../plugin/features.php
          return $framework->render_features_tab($config, $features_config);
        },
      ];
    }
  }

  if (isset($config['recommended_products'])
    && !empty($config['recommended_products'])
    && (!empty($config['recommended_products']['external']) || !empty($config['recommended_products']['tangible']))
  ) {
    $tabs['recommend'] = [
      'title' => 'Recommendations',
      'callback' => $framework->render_recommend_tab,
    ];
  }

  $connected_to_store = $config['item_id']!==false;

  if ($connected_to_store) {
    $tabs['license'] = [
      'title' => 'License',
      'callback' => $framework->render_license_tab,
    ];
  }

  if (!isset($config['setting_tabs']) || !isset($config['setting_tabs']['support'])) {
    $tabs['support'] = [
      'title' => 'Support',
      'callback' => $framework->render_support_tab,
    ];
  }

  if (isset($config['setting_tabs'])) {

    if (isset($config['setting_tabs'][0])) {
      // Convert to associative array
      $new_tabs = [];
      foreach ($config['setting_tabs'] as $key => $value) {
        $name = !empty($value['name']) ? $value['name'] : 'settings';
        $new_tabs[$name] = $value;
      }
      $config['setting_tabs'] = $new_tabs;
    }

    $tabs = array_merge($config['setting_tabs'], $tabs);
  }

  // First tab by default
  reset($tabs);
  $default_tab = key($tabs);

  $active_tab = $framework->is_settings_page($config)
    ? (isset($_GET['tab']) ? $_GET['tab'] : $default_tab)
    : ''
  ;

  $config['created_tabs'] = $tabs;
  $config['active_tab'] = $active_tab;

  return $config;
};
