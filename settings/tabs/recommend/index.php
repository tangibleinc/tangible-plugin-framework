<?php

$framework->recommended_product_presets = include __DIR__.'/product-presets.php';

$framework->render_recommend_tab = function($plugin) use ($framework) {

  $external_products = @$plugin['recommended_products']['external'];
  $tangible_products = @$plugin['recommended_products']['tangible'];

  ?><div class="tangible-recommendations">
    <?php

    if (!empty($external_products)) {
      ?>
      <h3 class="card-section-title">Plugins we recommend to use with <?php echo $plugin['title']; ?></h3>
      <?php
      $framework->render_recommend_cards($external_products);
    }

    if (!empty($tangible_products)) {
      ?>
      <h3 class="card-section-title">If you like <?php echo $plugin['title']; ?>, you might like our other plugins..</h3>
      <?php
      $framework->render_recommend_cards($tangible_products);
    }

    ?>
  </div><?php
};

$framework->render_recommend_cards = function($cards = []) use ($framework) {

  $product_presets = $framework->recommended_product_presets;

  foreach ($cards as $index => $card) {

    if (is_string($card)) $card = [ 'preset' => $card ];
    if (isset($card['preset'])) {
      $preset = $product_presets[ $card['preset'] ];
      $card = array_merge($preset, $card);
    }

    if ($index % 2 === 0) {
      ?><div class="card-row"><?php
    }

    ?>
    <div class="card">
      <div class="card-title"><?php echo $card['title']; ?></div>
      <div class="card-description">
        <?php echo $card['description']; ?>
      </div>
      <div class="card-call-to-action">
        <a href="<?php echo $card['url']; ?>"><button type="button" class="button button-primary">
          Get it now
        </button></a>
        <?php if (!empty($card['affiliate'])) {
          ?>
          <div class="card-call-to-action-info">
            This is an affiliate link
          </div>
          <?php
        } ?>
      </div>
    </div>
    <?php

    if ($index % 2 === 1) {
      ?></div><?php
    }
  }

  if ($index % 2 !== 1) {

    // Empty card for alignment
    ?><div class="card card--empty">&nbsp;</div><?php

    ?></div><?php
  }

};
