<?php

return [
  'beaverdash' => [
    'title' => 'BeaverDash',
    'description' => 'Meet BeaverDash, the easiest way to make Learndash beautiful.
BeaverDash bridges the gap between the best WordPress page builder and the best WordPress LMS.',
    'url' => "https://tangibleplugins.com/products/beaverdash/?utm_source=wp-admin&utm_medium=admin_notice&utm_campaign=plugin_links&utm_content=plugin_recommendation_cta",
  ],
  'learndash-materials' => [
    'title' => 'LearnDash Materials Extended',
    'description' => 'Easily add new course materials using drag and drop interface. Beacuase it works by adding a shortcode into the native LearnDash materials box, it retains compatibility with any existing setup.',
    'url' => "https://tangibleplugins.com/products/learndash-materials-extended/?utm_source=wp-admin&utm_medium=admin_notice&utm_campaign=plugin_links&utm_content=plugin_recommendation_cta",
  ],
  'memberdash' => [
    'title' => 'MemberDash',
    'description' => 'MemberDash is a straight-forward integration between WooCommerce Memberships and LearnDash. It functions very similarly to the integrations with WooCommerce Subscriptions and Products from the official LearnDash WooCommerce plugin.',
    'url' => "https://tangibleplugins.com/products/memberdash/?utm_source=wp-admin&utm_medium=admin_notice&utm_campaign=plugin_links&utm_content=plugin_recommendation_cta",
  ],
];
