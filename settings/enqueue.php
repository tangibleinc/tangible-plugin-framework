<?php

add_action('admin_enqueue_scripts', function () use ($framework) {

  if (!isset($_GET['page']) || empty($_GET['page'])) return;
  $page = $_GET['page'];

  $plugins = $framework->get_plugin_configs();
  if (empty($plugins)) return;

  $settings_css =  $framework->url.'/assets/settings.css';
  $settings_version =  $framework->version;
  $enqueued_settings = false;

  foreach ($plugins as $config) {

    if (!$framework->is_settings_page($config)) continue;

    // For all plugin settings pages
    if (!$enqueued_settings) {
      wp_enqueue_style('tangible-plugin-admin-settings-page-css', $settings_css, [], $settings_version);
      $enqueued_settings = true;
    }

    // Plugin-specific CSS, JS

    $name = $config['name'];
    $version = $config['version'];

    if (isset($config['settings_css'])) {

      $css = $config['settings_css'];

      if (!is_array($css)) $css = [ $css ];
      foreach ($css as $index => $this_css) {
        wp_enqueue_style("{$name}-admin-settings-css".($index > 0 ? '-'.($index+1) : ''), $this_css, [], $version);
      }
    }
    if (isset($config['settings_js'])) {

      $js = $config['settings_js'];

      if (!is_array($js)) $js = [ $js ];
      foreach ($js as $index => $this_js) {
        wp_enqueue_script("{$name}-admin-settings-js".($index > 0 ? '-'.($index+1) : ''), $this_js, ['jquery'], $version);
      }
    }

    do_action("tangible_admin_enqueue_{$config['name']}");

    $active_tab = $framework->get_active_settings_tab($config);

    if (empty($active_tab)) continue;

    // Tab-specific CSS, JS

    if (isset($config['created_tabs']) && isset($config['created_tabs'][$active_tab])) {

      $tab_config = $config['created_tabs'][$active_tab];

      if (isset($tab_config['css'])) {

        $css = $tab_config['css'];

        if (!is_array($css)) $css = [ $css ];
        foreach ($css as $index => $this_css) {
          wp_enqueue_style("{$name}-admin-tab-{$active_tab}-css".($index > 0 ? '-'.($index+1) : ''), $this_css, [], $version);
        }
      }

      if (isset($tab_config['js'])) {

        $js = $tab_config['js'];

        if (!is_array($js)) $js = [ $js ];
        foreach ($js as $index => $this_js) {
          wp_enqueue_script("{$name}-admin-tab-{$active_tab}-js".($index > 0 ? '-'.($index+1) : ''), $this_js, ['jquery'], $version);
        }
      }
    }

    do_action("tangible_admin_enqueue_{$config['name']}_tab_{$active_tab}");
  }
});
