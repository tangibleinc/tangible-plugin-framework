<?php

$framework->render_setting_field = function($config) use ($framework) {
  $render_name = "render_setting_field_{$config['type']}";
  if (!isset($framework->$render_name)) {
    throw new Exception("Unknown field \"{$config['type']}\"");
  }
  $fn = $framework->$render_name;
  return $fn($config);
};

$framework->render_setting_field_checkbox = function($config) use ($framework) {

  foreach ([
    'name',
    'value',
    'label'
  ] as $key) {
    $$key = $config[$key];
  }

  $checked = $value==='true';

  // This workaround is needed because an unchecked value doesn't get passed to form POST.
  // It toggles a hidden input field to ensure it gets saved.
  // https://stackoverflow.com/questions/1809494/post-unchecked-html-checkboxes#answer-25764926

  // TODO: Simplify by gathering form data with JS and provide an AJAX endpoint to save setting fields

  ?>
  <label>
  <input type="hidden" name="<?php echo $name; ?>" value="<?php echo $checked ? 'true' : 'false'; ?>" autocomplete="off"><input type="checkbox"
    value="true" autocomplete="off"
    onclick="this.previousSibling.value=this.previousSibling.value==='true'?'false':'true'"
    <?php echo $checked ? 'checked="checked"' : ''; ?>
  />
    <?php echo $label; ?>
  </label>
  <?php
};
