<?php

include __DIR__.'/render.php';

$framework->register_setting_fields = function($plugin) use ($framework) {
  $settings_key = $framework->get_settings_key($plugin);
  register_setting($settings_key, $settings_key);
};

// Option methods with multi-site
// These use "raw" setting key without prefix

$framework->get_setting = function($plugin, $key, $default = false) use ($framework) {
  return $framework->is_multisite($plugin)
    ? get_network_option(null, $key, $default)
    : get_option($key, $default);
};

$framework->update_setting = function($plugin, $key, $value) use ($framework) {
  return $framework->is_multisite($plugin)
    ? update_network_option(null, $key, $value)
    : update_option($key, $value);
};

$framework->delete_setting = function($plugin, $key) use ($framework) {
  return $framework->is_multisite($plugin)
    ? delete_network_option(null, $key)
    : delete_option($key);
};

// Setting fields - These use setting prefix

$framework->get_settings_key = function($plugin) use ($framework) {
  $prefix = @$plugin['setting_prefix'];
  return "{$prefix}_settings";
};

$framework->get_settings = function($plugin_or_api) use ($framework) {

  // Backward compatibility for beaverdash(-mu)
  $plugin = $framework->get_config($plugin_or_api);
  if (empty($plugin)) return [];

  $key = $framework->get_settings_key($plugin);
  $settings = $framework->get_setting($plugin, $key, []);
  if (empty($settings)) return [];
  return $settings;
};

/**
 * Update settings by merging new and existing settings' values
 */
$framework->update_settings = function($plugin, $settings, $reset = false) use ($framework) {
  $key = $framework->get_settings_key($plugin);
  $prev_settings = $framework->get_settings($plugin);
  return $framework->update_setting($plugin, $key,
    $reset
      ? $settings
      : array_merge($prev_settings, $settings)
  );
};

/**
 * Reset settings - Like update_settings, but overwrites whole settings object
 */
$framework->reset_settings = function($plugin, $settings) use ($framework) {
  $framework->update_settings($plugin, $settings, true);
};


// Setting page and tabs

$framework->get_settings_page_slug = function($plugin) use ($framework) {
  return "{$plugin['name']}-settings";
};

$framework->get_settings_page_url = function($plugin, $tab = '') use ($framework) {

  $slug = $framework->get_settings_page_slug($plugin);
  $url_base = $framework->get_settings_page_url_base($plugin);

  $tab_query = !empty($tab) ? "&tab=$tab" : '';
  $url = "{$url_base}?page={$slug}{$tab_query}";
  $is_multisite = $framework->is_multisite($plugin);

  return $is_multisite ? network_admin_url($url) : admin_url($url);
};

$framework->get_settings_page_url_base = function($plugin) use ($framework) {
  return isset($plugin['settings_menu'])
    // Main or submenu - @see ../settings/menu.php
    ? 'admin.php'
    : ($framework->is_multisite($plugin)
      ? 'settings.php'
      : 'options-general.php')
  ;
};


$framework->is_settings_page = function($config) use ($framework) {

  if (!is_admin() || !isset($_GET['page'])) return false;

  $page = $_GET['page'];
  $check_slug = $framework->get_settings_page_slug($config);

  return $page===$check_slug;
};

$framework->get_active_settings_tab = function($config) use ($framework) {
  return @$config['active_tab']; // Defined in ../tabs/create
};
