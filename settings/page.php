<?php

$framework->render_settings_page = function($config) use ($framework) {

  $name = $config['name'];
  $title = $config['title'];
  $prefix = $config['setting_prefix'];

  $tabs = $config['created_tabs'];
  $active_tab = $config['active_tab'];

  $settings_key = $framework->get_settings_key($config);
  $settings = $framework->get_settings($config);
  $nonce_key = "{$prefix}_nonce";

  // POST to current page, handled by $framework->settings_action in ./index
  $form_action_target = '';
  // Previously: $framework->is_multisite($config) ? str_replace(site_url(), '', $framework->get_settings_page_url($config)) : 'options.php';

  ?>
  <div class="wrap tangible-plugin-settings-page <?php echo $name; ?>-settings">

    <header>
      <div class="plugin-title">
        <h1>
          <?php
            if (isset($config['settings_title_callback'])) {
              $config['settings_title_callback']($config, $tabs, $active_tab);
            } elseif (isset($config['settings_header']['logo'])) {
              $type = $config['settings_header']['logo'];
              $url = $config['plugin_logos'][$type];
              ?>
            
              <div class="plugin-title-and-logo">
                <img class="plugin-logo<?= $config['settings_header']['logo'] === 'full' ? '-full' : '' ?>"
                  alt="MemberSync Logo"
                  src="<?= $url ?>"
                />
                <?php
                  if (!isset($config['settings_header']['show_title']) || $config['settings_header']['show_title'] !== false) {
                    echo $title;
                  }
                ?>
              </div>
            
              <?php
            } else {
              echo $title;
            }
          ?>
          <div class="tangible-plugin-store-link">
            By <a href="https://tangibleplugins.com" target="_blank">Tangible Plugins</a>
          </div>
        </h1>
      </div>
    </header>

    <h2 class="nav-tab-wrapper">
      <?php
      foreach ($tabs as $tab_slug => $tab) {

        $url = $framework->get_settings_page_url($config, $tab_slug);

        $classname = 'nav-tab';
        if ($tab_slug===$active_tab) $classname .= ' nav-tab-active';

        ?><a class="<?php echo $classname; ?>" href="<?php echo $url; ?>"><?php echo $tab['title']; ?></a><?php
      }
      ?>
    </h2>

    <div class="tangible-plugin-settings-section-wrapper">
      <?php if (!empty($titleSection = $tabs[$active_tab]['title_section'] ?? null)) { ?>
        <div class="tangible-plugin-settings-title-section">
          <h2><?= $titleSection['title'] ?? $tabs[$active_tab]['title'] ?></h2>
          <p><?= $titleSection['description'] ?? '' ?></p>
        </div>
      <?php } ?>
  
      <form method="post" action="<?php echo esc_attr_e($form_action_target); ?>"
        class="tangible-plugin-settings-tab <?php echo $name; ?>-settings-tab <?php echo $name; ?>-settings-tab-<?php echo $active_tab; ?>"
      >
        <?php
  
        if ($active_tab!=='license') {
          settings_fields($settings_key);
        }
  
        wp_nonce_field($nonce_key, $nonce_key);
  
        if (isset($tabs[$active_tab]) && isset($tabs[$active_tab]['callback'])) {
  
          // Render tab, passing plugin settings and key
  
          $tabs[$active_tab]['callback']($config, $settings, $settings_key);
        }
  
        ?>
      </form>
    </div>

  </div>
  <?php
};
