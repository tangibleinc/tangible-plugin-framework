<?php

$framework->register_admin_menu = function($config) use ($framework) {

  $plugin = $framework->get_api($config);

  $is_multisite = $plugin->is_multisite();

  $action = $is_multisite
    ? 'network_admin_menu'
    : 'admin_menu';

  add_action($action, function() use ($framework, $plugin, $is_multisite) {

    // Get config object after it's been updated by create_settings_tabs() in ../plugin/api
    $config = $plugin->config;

    $title = isset($config['settings_menu_title'])
      ? $config['settings_menu_title']
      : $config['title']
    ;

    $settings_page = "{$config['name']}-settings";

    $settings_page_callback = function() use ($framework, $config) {
      $framework->render_settings_page($config);
    };

    $user_capability = $is_multisite ? 'manage_network_plugins' : 'manage_options';

    // Pass settings page URL to plugin instance for shortcut
    $plugin->settings_page_url = $framework->get_settings_page_url($config);

    if (isset($config['settings_menu'])) {

      /**
       * Main menu item
       * @see https://developer.wordpress.org/reference/functions/add_menu_page
       */
      $menu_config = $config['settings_menu'];

      if (isset($menu_config['title'])) $title = $menu_config['title'];

      add_menu_page(
        $title, $title,
        $user_capability,
        $settings_page,
        $settings_page_callback,
        isset($menu_config['icon_url'])
          ? $menu_config['icon_url']
          : $framework->tangible_dashicon
        ,
        isset($menu_config['position']) ? $menu_config['position'] : null
      );

      return;
    }

    $url_base = $framework->get_settings_page_url_base($config);

    /**
     * Submenu page under Settings
     * @see https://developer.wordpress.org/reference/functions/add_submenu_page
     */
    add_submenu_page(
      $url_base,
      $title, $title,
      $user_capability,
      $settings_page,
      $settings_page_callback
    );
  });
};

// Tangible logo, Dashicon-style, for admin sidebar menu

$framework->tangible_dashicon = 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zeGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZpZXdCb3g9Ii01IC01IDEwOSAxMDkiPgoKICAgIDxwYXRoIGQ9Ik0wIDAgSCAzMyBWIDMzIEggMCBMIDAgMCIgZmlsbD0iIzllYTNhOCI+PC9wYXRoPgogICAgPHBhdGggZD0iTTMzIDAgSCA2NiBWIDMzIEggMzMgTCAzMyAwIiBmaWxsPSIjOWVhM2E4Ij48L3BhdGg+CiAgICA8cGF0aCBkPSJNNjYgMCBIIDk5IFYgMzMgSCA2NiBMIDY2IDAiIGZpbGw9IiM5ZWEzYTgiPjwvcGF0aD4KCiAgICA8cGF0aCBkPSJNMCAzMyBIIDMzIFYgNjYgSCAwIEwgMCAzMyIgZmlsbD0iIzllYTNhOCI+PC9wYXRoPgogICAgPHBhdGggZD0iTTY2IDMzIEggOTkgViA2NiBIIDY2IEwgNjYgMzMiIGZpbGw9IiM5ZWEzYTgiPjwvcGF0aD4KCiAgICA8cGF0aCBkPSJNMzMgNjYgSCA2NiBWIDk5IEggMzMgTCAzMyA2NiIgZmlsbD0iIzllYTNhOCI+PC9wYXRoPgoKICA8L3N2Zz4=';