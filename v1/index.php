<?php
namespace Tangible\PluginFramework;
/**
 * Globally namespaced functions for backward compatibility
 *
 * These can be removed after all Tangible plugins have migrated to use versioned instance of
 * plugin framework
 */

 function register_plugin($plugin) {
  return tangible_plugin_framework()->latest->register_plugin($plugin);
}

function get_settings($plugin) {
  return tangible_plugin_framework()->latest->get_settings($plugin);
}

function get_settings_key($plugin) {
  return tangible_plugin_framework()->latest->get_settings_key($plugin);
}

function get_settings_page_url($plugin, $tab = '') {
  return tangible_plugin_framework()->latest->get_settings_page_url($plugin, $tab);
}

function update_settings($plugin, $settings) {
  return tangible_plugin_framework()->latest->update_settings($plugin, $settings);
}

function is_admin_notice_dismissed($key) {
  return tangible_plugin_framework()->latest->is_admin_notice_dismissed($key);
}

function has_valid_license($plugin) {
  return tangible_plugin_framework()->latest->has_valid_license($plugin);
}
