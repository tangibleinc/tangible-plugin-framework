<?php

require __DIR__ . '/tangible-module.php';

if (!function_exists('tangible_plugin_framework')) {
  require_once __DIR__.'/core.php';
}

/**
 * Global shortcuts for latest version instance
 *
 * The function tangible_plugin_framework is reserved to be backward compatible.
 */

if (!function_exists('tangible')) {
  function tangible( $callback = false ) {
    return $callback === false
      ? tangible_plugin_framework()->latest
      : tangible_plugin_framework()->latest->on_ready( $callback )
    ;
  }
}

if (!function_exists('tgbl')) {
  function tgbl( $callback = false ) {
    return tangible( $callback );
  }
}

new class extends TangibleModule {

  public $name = 'tangible_plugin_framework';
  public $version = '20240801';
  public $state = [];
  public $plugins = [];

  static $tmp;

  function __construct() {

    $this->file_path = __FILE__;
    $this->url = plugins_url('', __FILE__);

    /**
     * Store URL is used for license management and plugin updater, if the plugin has a corresponding
     * product item ID in its configuration.
     *
     * @see init.php, updater and store folders
     */
    $this->store_url     = 'https://tangibleplugins.com';
    $this->support_email = 'service@tangibleplugins.com';

    // Temporary state that is not logged with $framework->see
    self::$tmp = new stdClass;

    parent::__construct();

    /**
     * For backward compatibility: Provide ready hook on tangible_plugin_framework()->ready
     */
    $this->core = tangible_plugin_framework();
    $this->core_ready_hook = $this->core->ready = "{$this->ready}__core";

    $is_latest_so_far = !isset($this->core->latest);

    if ($is_latest_so_far) {
      $this->core->latest = $this;
    }

    /**
     * Simpler hook for when all modules and plugin framework ready.
     *
     * Use tangible( $callback ) at any time before or after, to get the latest version instance.
     */
    $this->ready_hook = $this->name;
    $this->ready_hook_done = false;

    $framework = $this;

    // Called by theme
    if (did_action('plugins_loaded') && $this->is_latest_version()) {
      $this->all_versions_loaded();
      if ($is_latest_so_far) $this->plugins_loaded();
    }
  }

  function load_version() {

    $framework = $this;

    /**
     * Existing features loaded here to be backward compatible with the use of
     * a specific version of framework.
     */
    require_once __DIR__.'/features.php';

    /**
     * Schedule action after all TangibleModule instances are loaded.
     *
     * Using a trick here to avoid increasing priority to 7, which is already
     * being used in some Tangible plugins.
     */
    add_action('plugins_loaded', function() {
      add_action('plugins_loaded', function() {
        $this->plugins_loaded();
      }, 6); // This should schedule it after all other priority 6 actions
    }, 5); // Just before
  }

  function load_latest_version() {

    // Provide latest version via tangible_plugin_framework()->latest
    $this->core->latest = $this->latest;
  }

  function plugins_loaded() {

    do_action( $this->core_ready_hook, $this );

    if ( ! $this->is_latest_version() ) return;

    $framework = $this;

    // Gather all plugin API instances

    $module_versions = array_merge(
      isset($this->core->state['module_versions']) // Old loading method
        ? $this->core->state['module_versions']
        : []
      ,
      $this->module_state['versions'] // New
    );

    $plugins = [];
    foreach ($module_versions as $f) {

      $apis = $f->state['plugin_api'];

      foreach ($apis as $key => $api) {
        $plugins[$key] = $api;
      }

      /**
       * For branch "variant/without-updater"
       * Check for other Tangible plugins that may need to include the EDD updater.
       */
      if ($f!==$this // Ignore self
        && isset($framework->replaced_updater_version) // Latest version missing updater
        // This instance has updater and is highest version so far
        && !isset($f->replaced_updater_version)
        && version_compare( $f->version, $framework->replaced_updater_version ) > 0
        && file_exists( $updater_path = dirname($f->file_path) . '/updater/index.php' )
      ) {
        include $updater_path;
        $framework->replaced_updater_version = $f->version;
        // tangible()->see('Updater included', $framework->replaced_updater_version, $f->file_path);
      }
    }

    ksort($plugins);
    $this->plugins = $plugins;

    // $this->see( $plugins );

    /**
     * New framework features are added here, so plugins always use the latest version.
     */

    require_once __DIR__.'/modules/index.php';

    /**
     * All modules and latest version of plugin framework are ready.
     *
     * This is called on `plugins_loaded` action, at the end of priority 6,
     * which lets plugins hook on priority 10 (default) and call register_plugin().
     */

    $this->ready_hook_done = true;

    do_action( 'tangible_modules_ready', $this );
    do_action( $this->ready_hook, $this );

    // Make sure to call it only once
    remove_all_actions( $this->ready_hook );
  }

  function on_ready( $fn ) {

    if ($this->ready_hook_done) return $fn(
      $this->get_latest_version()
    );

    add_action($this->ready_hook, function( $latest ) use ( $fn ) {
      return $fn( $latest );
    });
  }
};
