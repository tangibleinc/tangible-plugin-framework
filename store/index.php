<?php

$framework->get_store_url = function() use ($framework) {
  return $framework->store_url;
};

$framework->store_action = function($config, $action, $data) use ($framework) {

  $item_id = $config['item_id']; // Product ID in EDD store
  $store_url = $framework->get_store_url();
  $api = $framework->get_api($config);

  $api->log('store_action', $store_url, $config['name'].' (ID #'.$item_id.') - '.$action, $data);

  return wp_remote_post($store_url, [
    'timeout' => 60, // Originally 15
    'sslverify' => false,
    'body' => $data+[
      'edd_action' => $action,
      'item_id'  => urlencode($item_id),
      'url'        => home_url()
    ]
  ]);
};
