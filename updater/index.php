<?php

$framework->register_updater = function($plugin) use ($framework) {

  $store_url = $framework->get_store_url();
  $license = $framework->get_license($plugin);
  $api = $framework->get_api($plugin);

  if ( ! class_exists('Tangible\\Updater\\EDDPluginUpdater') ) {
    require_once __DIR__.'/edd-plugin-updater/index.php';
  }

  return new Tangible\Updater\EDDPluginUpdater($store_url, $plugin['file_path'], [
    'item_id'   => $plugin['item_id'],
    'version'   => $plugin['version'],
    'license'   => $license,
    'author'    => 'Tangible Inc',
    'log' => !$api->debug_log_enabled() ? false : function() use ($api) {
      $args = func_get_args();
      call_user_func_array([$api, 'log'], @$args[0]);
    },
  ]);
};

$framework->register_theme_updater = function($theme) use ($framework) {

  $store_url = $framework->get_store_url();
  $api = $framework->get_theme_api($theme);

  if ( ! class_exists('Tangible\\Updater\\EDDThemeUpdaterConfig') ) {
    require_once __DIR__.'/edd-theme-updater/index.php';
  }

  return new Tangible\Updater\EDDThemeUpdaterConfig([
    'remote_api_url' => $store_url, // Site where EDD is hosted.
    'item_name'      => $theme['title'], // Name of theme.
    'theme_slug'     => $theme['name'], // Theme slug.
    'version'        => $theme['version'], // The current version of this theme.
    // 'license'        => '', // @see edd-theme-updater/EDDThemeUpdaterAdmin
    'author'         => 'Tangible Inc', // The author of this theme.
    'item_id'        => $theme['item_id'],
    'download_id'    => $theme['item_id'], // Optional, used for generating a license renewal link.
    'renew_url'      => '', // Optional, allows for a custom license renewal link.
  ]);
};
