<?php

$framework->is_dependency_active = function($basename, $fallback_check = false) {

  // Use fallback check first, because it's probably faster than is_plugin_active()

  return ($fallback_check && $fallback_check())
    || ( function_exists('is_plugin_active')
      ? is_plugin_active($basename) // Admin side
      : in_array($basename, apply_filters('active_plugins', get_option('active_plugins'))) // Frontend
    )
  ;
};

$framework->get_missing_dependencies = function($plugin) use ($framework) {

  if (!isset($plugin['dependencies'])) return [];

  // Dependencies is a map of 'plugin-directory/plugin-file.php' => $deps_config
  $deps = $plugin['dependencies'];
  $missing_deps = [];

  foreach ($deps as $basename => $config) {

    $fallback_check = isset($config['fallback_check'])
      ? $config['fallback_check']
      : false
    ;

    if ($framework->is_dependency_active($basename, $fallback_check)) continue;

    $missing_deps []= $config;
  }

  return $missing_deps;
};

$framework->has_all_dependencies = function($plugin) use ($framework) {
  return empty($framework->get_missing_dependencies( $plugin ));
};

$framework->register_dependency_check = function($plugin) use ($framework) {

  if (!isset($plugin['dependencies'])) return;

  $action = $framework->is_multisite($plugin)
    ? 'network_admin_notices'
    : 'admin_notices';

  add_action($action, function() use ($framework, $plugin) {

    $missing_deps = $framework->get_missing_dependencies( $plugin );

    if (empty($missing_deps)) return;

    // notice-error, notice-warning, notice-success, notice-info, is-dismissible
    ?>
    <div class="notice notice-warning">
      <?php

      if (isset($plugin['missing_dependencies_message'])) {

        ?><?php echo $plugin['missing_dependencies_message']($plugin, $missing_deps); ?><?php

      } else {

        ?><p><b>Missing plugin dependencies for <?php echo $plugin['title']; ?></b></p><?php

        ?><p><?php echo $plugin['title']; ?> won't work properly until the following plugins have been installed and activated.</p><?php

        ?><p><?php
        foreach ($missing_deps as $dep) {
          ?><?php
            if (isset($dep['url'])) {

              ?><a href="<?php echo $dep['url']; ?>" target="_blank"><?php echo $dep['title']; ?></a><?php

            } else {
              ?><?php echo $dep['title']; ?><?php
            }
          ?><br><?php
        }
        ?></p><?php
      }

      ?>
    </div>
    <?php

  });
};
