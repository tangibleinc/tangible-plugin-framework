<?php

// Core: global, shared among plugins and backward-compatible

function tangible_plugin_framework() {
  static $o;
  if ($o) return $o;
  return $o = new class {
    public $state;
    public $ready;
    public $latest; // Latest version's module instance
  };
}

tangible_plugin_framework()->state = [
  'module_versions' => [],
];

// Previously, the module checked if this class exists before loading
class TangiblePluginFramework {}

require __DIR__.'/v1/index.php';
