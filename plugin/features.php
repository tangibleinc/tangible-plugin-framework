<?php

// Load enabled plugin features based on settings

$framework->get_plugin_feature_key = function($features_config, $feature) {
  return "{$features_config['setting_prefix']}_{$feature['name']}";
};

$framework->is_plugin_feature_enabled = function($features_config, $feature, $settings) use ($framework) {

  $feature_key = $framework->get_plugin_feature_key($features_config, $feature);
  $default_value = isset($feature['default']) ? $feature['default'] : false; // Disabled by default

  return !isset($settings[$feature_key])
    ? $default_value
    : $settings[$feature_key]==='true';
};

$framework->get_plugin_feature_settings_key_index = function($features_config, $feature) use ($framework) {
  $feature_key = $framework->get_plugin_feature_key($features_config, $feature);
  return "{$feature_key}_settings";
};

$framework->get_plugin_feature_settings_key = function($plugin_config, $features_config, $feature) use ($framework) {
  $settings_key = $framework->get_settings_key($plugin_config);
  $index = $framework->get_plugin_feature_settings_key_index($features_config, $feature);
  return "{$settings_key}[{$index}]";
};

$framework->get_plugin_feature_settings = function($plugin_config, $features_config, $feature) use ($framework) {
  $settings = $framework->get_settings($plugin_config);
  $index = $framework->get_plugin_feature_settings_key_index($features_config, $feature);
  return isset($settings[$index]) ? $settings[$index] : [];
};

$framework->get_plugin_feature_by_name = function($plugin_config, $feature_name) use ($framework) {

  if (!isset($plugin_config['feature_groups']) || empty($plugin_config['feature_groups'])) return false;

  foreach ($plugin_config['feature_groups'] as $features_config) {

    $features = @$features_config['features'];
    if (empty($features)) continue;

    foreach ($features as $feature) {
      if ($feature['name']!==$feature_name) continue;
      // Pass group config for convenience
      $feature['group'] = $features_config;
      return $feature;
    }
  }

  return false;
};

$framework->get_plugin_feature_settings_by_name = function($plugin_config, $feature_name) use ($framework) {
  $feature = $framework->get_plugin_feature_by_name($plugin_config, $feature_name);
  if ($feature===false) return [];
  return $framework->get_plugin_feature_settings($plugin_config, $feature['group'], $feature);
};


$framework->update_plugin_feature_settings_by_name = function($plugin_config, $feature_name, $feature_settings)
  use ($framework) {

  $feature = $framework->get_plugin_feature_by_name($plugin_config, $feature_name);
  if ($feature===false) return [];

  $features_config = $feature['group'];

  $settings = $framework->get_settings($plugin_config);
  $index = $framework->get_plugin_feature_settings_key_index($features_config, $feature);

  $settings[$index] = $feature_settings;

  $framework->update_settings($plugin_config, $settings);

  return $feature_settings;
};


// @see ../init.php
$framework->load_plugin_features = function($plugin_config) use ($framework) {

  if (!isset($plugin_config['feature_groups']) || empty($plugin_config['feature_groups'])) return;

  $plugin = $framework->get_api($plugin_config);
  $settings = $plugin->get_settings();

  foreach ($plugin_config['feature_groups'] as $features_config) {

    $features_path = @$features_config['path'];

    $features = @$features_config['features'];
    if (empty($features)) continue;

    foreach ($features as $feature) {
      if (!$framework->is_plugin_feature_enabled($features_config, $feature, $settings)) continue;

      // Include feature - passing $framework, $plugin, $feature

      $feature_entry_file = isset($feature['entry_file'])
        ? "{$feature['name']}/{$feature['entry_file']}"
        : "{$feature['name']}/index.php"
      ;

      include $features_path."/$feature_entry_file";
    }
  }
};

// @see ../settings/page.php
$framework->render_features_tab = function($plugin_config, $features_config) use ($framework) {

  $settings_key = $framework->get_settings_key($plugin_config);
  $settings = $framework->get_settings($plugin_config);

  $features = $features_config['features'];
  $features_title = $features_config['title'];

?>
  <div class="tangible-plugin-features-settings tangible-plugin-<?php echo $plugin_config['name']; ?>-features-settings">
    <h2><?php echo $features_title; ?></h2>
    <?php
      foreach ($features as $feature) {
        $name = $feature['name'];
        $title = $feature['title'];
        $feature_key = $framework->get_plugin_feature_key($features_config, $feature);
        $is_enabled = $framework->is_plugin_feature_enabled($features_config, $feature, $settings);
        ?>
        <div class="setting-row feature-<?php echo $name; ?>">
          <?php

            ?><div class="feature-title"><?php

            $framework->render_setting_field([
              'type' => 'checkbox',
              'name' => "{$settings_key}[$feature_key]",
              'value' => $is_enabled ? 'true' : '',
              'label' => $title,
            ]);

            ?></div><?php

            if (isset($feature['description'])) {

              ?><div class="feature-description"><?php

              if (is_callable($feature['description'])) {

                $feature_settings = $framework->get_plugin_feature_settings($plugin_config, $features_config, $feature);
                $feature_settings_key = $framework->get_plugin_feature_settings_key($plugin_config, $features_config, $feature);

                $feature['description']($framework, $feature_settings, $feature_settings_key, $is_enabled);

              } else {
                echo $feature['description'];
              }

              ?></div><?php
            }
          ?>
        </div>
        <?php
      }
      submit_button();
    ?>
  </div><?php

};
