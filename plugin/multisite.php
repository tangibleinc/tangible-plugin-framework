<?php

$framework->is_multisite = function($config = []) use ($framework) {
  return \is_multisite() && isset($config['multisite']) && $config['multisite'];
};
