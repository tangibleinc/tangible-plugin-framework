<?php

$framework->state['plugins'] = [];
$framework->state['plugin_api'] = [];

$framework->register_plugin = function($config = []) use ($framework) {

  // Defaults
  foreach ([
    'item_id' => false,
    'multisite' => false,
  ] as $key => $value) {
    if (!isset($config[$key])) $config[$key] = $value;
  }

  // Validate config
  $required_keys = ['name', 'title', 'version', 'setting_prefix', 'file_path'];

  foreach ($required_keys as $key) {
    if (isset($config[$key])) continue;
    throw new \Exception("Tangible\PluginFramework\\register_plugin - Plugin \"{$config['name']}\" is missing required key \"$key\"");
  }

  $api = $framework->create_plugin_api($config);

  // Keep object reference to allow later modification
  $framework->state['plugins'] []= &$api->config;
  $framework->state['plugin_api'][ $config['name'] ] = $api;

  return $api;
};

$framework->get_plugin_configs = function() use ($framework) {
  return $framework->state['plugins'];
};

$framework->get_plugins = function() use ($framework) {
  return $framework->state['plugin_api'];
};

$framework->get_name = function($plugin) use ($framework) {
  return is_string($plugin)
    ? $plugin
    : (is_array($plugin)
      ? $plugin['name']
      : @$plugin->config['name']
    );
};

$framework->get_api = function($plugin) use ($framework) {
  $name = $framework->get_name($plugin);
  $plugins = $framework->get_plugins();
  if (isset($plugins[$name])) return $plugins[$name];
  // Not found - Check registered themes
  return $framework->get_theme_api($plugin);
};

$framework->get_config = function($plugin) use ($framework) {
  $api = $framework->get_api($plugin);
  if (empty($api)) return [];
  return $api->config;
};
