<?php

$framework->create_plugin_api = function($config) use ($framework) {
  $api = new class extends stdClass {

    use TangibleObject;

    // Bind instance to added methods
    // For backward compatibility with older TangibleObject trait
    function __set( $name, $value ) {
      $this->$name = is_object($value) && ($value instanceof Closure) ? $value->bindTo( $this, $this ) : $value;
    }

    public $name = 'tangible_plugin_api';
    public $config;

    /**
     * Create and configure plugin interface for the framework
     *
     * @param [type] $config [ name, title, version, file_path ]
     * @return $this
     */
    function init($config) {

      // Fill in any missing properties
      if (isset($config['file_path'])) {

        // __FILE__ of plugin entry file
        $file_path = $config['file_path'];

        if (!isset($config['url'])) {
          $config['url'] = plugins_url( '/', $file_path );
        }
        if (!isset($config['dir_path'])) {
          $config['dir_path'] = plugin_dir_path( $file_path );
        }
        if (!isset($config['base_path'])) {
          $config['base_path'] = plugin_basename( $file_path );
        }
      }

      // For convenience: $plugin->name, version, etc.
      foreach ($config as $key => $value) {
        $this->{$key} = $value;
      }

      global $wp_version;

      $config['wp_version'] = $wp_version;
      $config['php_version'] = phpversion();

      $config['log_file_path'] = dirname($config['file_path'])."/{$config['name']}.log";

      $this->config = $config+[

        // Defaults

        'dependencies' => [],

        // 'admin_notice' => '',

        // 'settings_css' => '',
        // 'settings_js' => '',
        // 'settings_title_callback' => '',

        'setting_tabs' => [
          // name => [ title => '', callback => '' ]
        ],

        'recommended_products' => [
          // tangible => [ Product, .. ]
          // external => []
        ],

        'feature_groups' => [
          // title, path, setting_prefix, features
        ],
      ];

      $this->framework = $config['framework'];

      // Must be before admin_menu, when plugin settings screen is loaded in ../settings/menu
      if (is_admin()) {
        add_action('wp_loaded', [$this, 'create_settings_tabs']);
      }

      return $this;
    }

    function create_settings_tabs($screen) {
      $this->config = $this->framework->create_plugin_settings_tabs($this->config);
    }

    // Config

    function register_admin_notice($fn) {
      $this->config['admin_notice'] = $fn;
      return $this;
    }

    function register_action_links($links) {
      if (!isset($this->config['action_links'])) {
        $this->config['action_links'] = [];
      }
      $this->config['action_links'] = array_merge($this->config['action_links'], $links);
      return $this;
    }

    function register_settings($settings) {

      // settings_*
      foreach ([
        'css',
        'js',
        'title_callback',
        'header',
        'menu',
        'menu_title' // Backward compatibility
      ] as $key) {
        if (!isset($settings[$key])) continue;
        $this->config["settings_{$key}"] = $settings[$key];
      }

      // setting_*
      foreach ([
        'tabs'
      ] as $key) {
        if (!isset($settings[$key])) continue;
        $this->config["setting_{$key}"] = $settings[$key];
      }

      return $this;
    }

    function register_settings_menu($menu_config = []) {
      return $this->register_settings([ 'menu' => $menu_config ]);
    }

    function register_setting_tabs($tabs) {
      return $this->register_settings([ 'tabs' => $tabs ]);
    }

    // Recommended products

    function register_recommended_products($products, $key = '') {

      // All product types
      if (empty($key)) {
        foreach (['external', 'tangible'] as $this_key) {
          if (!isset($products[$this_key])) continue;
          $this->register_recommended_products($products[$this_key], $this_key);
        }
        return;
      }

      // Individual product types
      if (!isset($this->config['recommended_products'][$key])) {
        $this->config['recommended_products'][$key] = [];
      }
      foreach ($products as $p) {
        $this->config['recommended_products'][$key] []= $p;
      }
      return $this;
    }

    function register_recommended_tangible_products($products) {
      return $this->register_recommended_products($products, 'tangible');
    }

    function register_recommended_external_products($products) {
      return $this->register_recommended_products($products, 'external');
    }

    // Settings

    function get_settings() {
      return $this->framework->get_settings($this->config);
    }

    function get_settings_key() {
      return $this->framework->get_settings_key($this->config);
    }

    function get_settings_page_url($tab = '') {
      return $this->framework->get_settings_page_url($this->config, $tab);
    }

    function update_settings($settings) {
      return $this->framework->update_settings($this->config, $settings);
    }

    function reset_settings($settings) {
      return $this->framework->reset_settings($this->config, $settings);
    }

    function is_settings_page() {
      return $this->framework->is_settings_page($this->config);
    }

    function is_settings_tab($tab) {
      return $this->is_settings_page()
        && isset($_GET['tab'])
        && $_GET['tab'] === $tab
      ;
    }

    function is_multisite() {
      return $this->framework->is_multisite($this->config);
    }

    // License - See ../license/fields.php

    function get_license() {
      return $this->framework->get_license($this->config);
    }

    function has_valid_license() {
      return $this->framework->has_valid_license($this->config);
    }

    function get_license_page_url() {
      return $this->framework->get_license_page_url($this->config);
    }

    function is_license_page() {
      return $this->is_settings_tab('license');
    }

    // Optional plugin features

    function register_features($features_config) {

      // Framework adds setting tabs for features in ../settings/page.php

      $group_index = count($this->config['feature_groups']);
      $this->config['feature_groups'] []= $features_config+[
        // Defaults
        'title' => 'Features',
        // Setting prefix must be unique
        'setting_prefix' => isset($features_config['title'])
          ? $this->framework->slugify($features_config['title'])
          : 'features',
      ];
      return $this;
    }

    function get_feature_settings($feature_name) {
      return $this->framework->get_plugin_feature_settings_by_name($this->config, $feature_name);
    }

    function update_feature_settings($feature_name, $settings) {
      return $this->framework->update_plugin_feature_settings_by_name($this->config, $feature_name, $settings);
    }

    // Plugin groups - see ../plugin-group

    function register_plugin_group($group_name) {
      return $this->framework->register_plugin_group($group_name, $this);
    }

    // Dependencies

    function register_dependencies($deps) {
      foreach ($deps as $key => $value)  {
        $this->config['dependencies'][$key] = $value;
      }
      return $this;
    }

    function register_missing_dependencies_message($fn) {

      // This is used in ../dependency-check

      $this->config['missing_dependencies_message'] = $fn;
      return $this;
    }

    function has_all_dependencies() {
      return $this->framework->has_all_dependencies( $this->config );
    }

    function get_missing_dependencies() {
      return $this->framework->get_missing_dependencies( $this->config );
    }

    // Utility functions: stats, log, debug

    function debug_stats() {

      $stats = $this->stats();

      ob_start();

?>Plugin: <?php echo $stats['pluginTitle']; ?> <?php echo $stats['pluginVersion']; ?>

Plugin framework: <?php echo $stats['pluginFrameworkVersion']; ?> in <?php echo $stats['pluginFrameworkDir']; ?>

Dependencies: <?php
  if (empty($stats['pluginDependencies'])) {
    ?>None<?php
  } else {
    foreach ($stats['pluginDependencies'] as $dep) {
      ?>

  <?php echo $dep['title']; ?> <?php echo $dep['version']; ?> - <?php echo $dep['active'] ? 'ACTIVE' : 'NOT ACTIVE'; ?>
<?php
    }
  }
?>

Theme: <?php echo $stats['themeTitle']; ?> <?php echo $stats['themeVersion']; ?><?php
  if (isset($stats['parentThemeTitle'])) {
    ?>, <?php echo $stats['parentThemeTitle']; ?> <?php echo $stats['parentThemeVersion']; ?><?php
  }
?>

Environment: WordPress <?php echo $stats['wordpressVersion']; ?> PHP <?php echo $stats['phpVersion']; ?>
<?php

      return ob_get_clean();
    }

    function stats() {

      global $wp;

      $config = $this->config;

      $stats = [
        // Source

        'timestamp' => time(),
        'siteUrl' => site_url(),
        //'currentUrl' => home_url( add_query_arg([], $wp->request )),

        // Environment

        'wordpressVersion' => $config['wp_version'],
        'phpVersion' => $config['php_version'],
        'pluginFrameworkVersion' => $this->framework->version,
        'pluginFrameworkDir' => plugin_basename(plugin_dir_path($this->framework->file_path)),
      ];

      // Theme

      $theme = wp_get_theme();

      $stats['themeName'] = $theme->stylesheet;
      $stats['themeTitle'] = $theme->display('Name');
      $stats['themeVersion'] = $theme->display('Version');

      $parent_theme = $theme->parent();
      if (!empty($parent_theme)) {
        $stats['parentThemeName'] = $parent_theme->template;
        $stats['parentThemeTitle'] = $parent_theme->display('Name');
        $stats['parentThemeVersion'] = $parent_theme->display('Version');
      }

      // Plugin

      foreach ([
        'name', 'title', 'version'
      ] as $key) {
        $field = 'plugin' . ucfirst( $key );
        $stats[ $field ] = $config[ $key ];
      }

      // Dependencies

      $stats['pluginDependencies'] = [];

      if ( isset($config['dependencies']) && !empty($config['dependencies']) ) {

        foreach ($config['dependencies'] as $dep_basename => $dep_config) {

          $fallback_check = isset($dep_config['fallback_check'])
            ? $dep_config['fallback_check']
            : false
          ;

          $is_dependency_active = $this->framework->is_dependency_active(
            $dep_basename,
            $fallback_check
          );

          $dep_file_path = WP_CONTENT_DIR."/plugins/$dep_basename";

          $dep_data = is_plugin_active($dep_basename)
            ? @get_plugin_data($dep_file_path, false)
            : [
              'Title' => @$dep_config['title'],
              'Version' => '0.0.0', // Unknown
            ];

          $stats['pluginDependencies'] []= [
            'name' => dirname( $dep_basename ),
            'title' => $dep_data['Title'],
            'version' => $dep_data['Version'],
            'active' => $is_dependency_active,
          ];
        }
      }

      return $stats;
    }

    function debug_log_enabled() {
      $plugin_settings = $this->get_settings();
      return isset($plugin_settings['debug_log']) && $plugin_settings['debug_log']==='true';
    }

    function analytics_log_enabled() {
      $plugin_settings = $this->get_settings();
      return isset($plugin_settings['analytics_log']) && $plugin_settings['analytics_log']==='true';
    }
  };

  // Using init instead of constructor, to pass variables from outer scope
  $api->init($config+[
    'framework' => $framework,
  ]);

  // Defined as dynamic method to allow shortcut: $log = $api->log;
  $api->log = function() {

    if ( ! $this->debug_log_enabled() ) return;

    $config = $this->config;

    $datetime = date('Y-m-d H:i:s');
    //$debug_stats = $this->debug_stats();

    // Metadata
    $log_meta = "\n---------- {$datetime} UTC {$config['name']}"; // {$debug_stats}\n

    $args = func_get_args();
    array_unshift($args, $config['log_file_path'], $log_meta);

    // Log to file - See ../utils/log.php
    call_user_func_array($this->framework->log_to_file, $args);
  };

  $api->log_to_analytics = function( $type, $message = '', $data = [] ) use ($framework, $api) {

    if ( ! $this->analytics_log_enabled() ) return;

    if (is_array($type)) {
      $data = $type;
    } else {
      $data = array_merge(
        [
          'type' => $type,
          'message' => $message,
        ],
        $data
      );
    }

    $stats = $api->stats();
    unset($stats['timestamp']); // Server adds timestamp by default

    return $framework->log_to_analytics(array_merge(
      $data,
      $stats
    ));
  };

  return $api;
};
