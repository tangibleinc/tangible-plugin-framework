<?php

add_action('plugins_loaded', function () use ($framework) {

  $configs = $framework->get_plugin_configs();
  if (empty($configs)) return;

  foreach ($configs as $config) {

    $framework->register_admin_menu($config);
    $framework->settings_action($config); // Must run before loading features
    $framework->load_plugin_features($config);

  }
}, 99); // After all register_plugin called

// Themes
add_action('after_setup_theme', function() use ($framework) {

  $theme_configs = $framework->get_theme_configs();
  $store_available = !empty($framework->get_store_url());

  foreach ($theme_configs as $config) {

    if (!$store_available || $config['item_id']===false) continue;

    // Theme updater and license

    $framework->register_theme_updater($config);
  }
});

// This runs after the above
add_action('admin_init', function() use ($framework) {

  $configs = $framework->get_plugin_configs();
  $store_available = !empty($framework->get_store_url());

  foreach ($configs as $config) {

    $framework->register_setting_fields($config);
    $framework->register_admin_notices($config);
    $framework->register_dependency_check($config);
    $framework->register_action_links($config);

    if (!$store_available || $config['item_id']===false) continue;

    // Plugin updater and license

    $framework->register_updater($config);
    $framework->register_license_field($config);
    $framework->register_license_notice($config);
    $framework->license_action($config);
  }

}, 0);
