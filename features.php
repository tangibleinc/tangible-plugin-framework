<?php

// Features are added to module instance. All functions and classes
// **must be anonymous** to support multiple versions being loaded.

include __DIR__.'/utils/index.php';

include __DIR__.'/admin-notices/index.php';
include __DIR__.'/dependency-check/index.php';
include __DIR__.'/license/index.php';
include __DIR__.'/plugin/index.php';
include __DIR__.'/plugin-group/index.php';
include __DIR__.'/settings/index.php';
include __DIR__.'/store/index.php';
include __DIR__.'/theme/index.php';
include __DIR__.'/updater/index.php';

include __DIR__.'/init.php';
