<?php

$framework->create_theme_api = function($config) use ($framework) {

  $api = new class extends stdClass {

    use TangibleObject;

    static $framework;

    public $name = 'tangible_theme_api';
    public $config;

    /**
     * Create and configure theme interface for the framework
     *
     * @param [type] $config [ name, title, version, file_path ]
     * @return $this
     */
    function init($config) {

      // For convenience: $theme->name, version, etc.
      foreach ($config as $key => $value) {
        $this->{$key} = $value;
      }

      $this->config = $config;

      self::$framework = tangible();

      return $this;
    }
  };

  $api->init($config);

  return $api;
};
