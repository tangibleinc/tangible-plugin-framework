<?php

$framework->state['themes'] = [];
$framework->state['theme_api'] = [];

$framework->register_theme = function($config = []) use ($framework) {

  // Defaults
  foreach ([
    'name' => 'unknown',
    'item_id' => false,
    'multisite' => false,
  ] as $key => $value) {
    if (!isset($config[$key])) $config[$key] = $value;
  }

  // Validate config
  $required_keys = [
    'name', 'title', 'version', 'file_path',
    // 'setting_prefix' // Unused
  ];

  foreach ($required_keys as $key) {
    if (isset($config[$key])) continue;
    throw new \Exception("Tangible\PluginFramework\\register_theme - Theme \"{$config['name']}\" is missing required key \"$key\"");
  }

  $api = $framework->create_theme_api($config);

  // Keep object reference to allow later modification
  $framework->state['themes'] []= &$api->config;
  $framework->state['theme_api'][ $config['name'] ] = $api;

  return $api;
};

$framework->get_theme_configs = function() use ($framework) {
  return $framework->state['themes'];
};

$framework->get_themes = function() use ($framework) {
  return $framework->state['theme_api'];
};

$framework->get_theme_name = function($theme) use ($framework) {
  return is_string($theme)
    ? $theme
    : (is_array($theme)
      ? $theme['name']
      : @$theme->config['name']
    );
};

$framework->get_theme_api = function($theme) use ($framework) {
  $name = $framework->get_theme_name($theme);
  $themes = $framework->get_themes();
  if (isset($themes[$name])) return $themes[$name];
};

$framework->get_theme_config = function($theme) use ($framework) {
  $api = $framework->get_theme_api($theme);
  if (empty($api)) return [];
  return $api->config;
};
