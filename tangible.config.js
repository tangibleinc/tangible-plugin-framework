module.exports = {
  build: [
    {
      task: 'js',
      src: 'assets/src/ajax/index.js',
      dest: 'assets/build/ajax.min.js',
      watch: 'assets/src/ajax/index.js'
    },
    {
      task: 'js',
      src: 'assets/src/preact/index.js',
      dest: 'assets/build/preact.min.js',
      watch: 'assets/src/preact/index.js'
    },
    {
      task: 'js',
      src: 'assets/src/tester/index.js',
      dest: 'assets/build/tester.min.js',
      watch: 'assets/src/tester/index.js'
    },
  ]
}