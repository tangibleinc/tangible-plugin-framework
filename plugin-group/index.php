<?php

/**
 * Plugin groups is a way to organize and share functionality among related plugins
 *
 * @see https://docs.tangible.one/modules/plugin-framework/plugin-groups
 */

 // Shared state among plugins using different framework versions
$framework_shared_state = &tangible_plugin_framework()->state;

$framework->plugin_group_state = $framework_shared_state['plugin_group'] =
  isset($framework_shared_state['plugin_group'])
    ? $framework_shared_state['plugin_group']
    : [
      'groups' => [],
      'is_loaded' => false,
    ]
;

$framework->get_plugin_group = function($group_name) use ($framework) {
  return @$framework->plugin_group_state['groups'][$group_name];
};

$framework->register_plugin_group = function($group_name, $plugin) use ($framework) {

  $group = $framework->get_plugin_group($group_name);

  if (empty($group)) {
    $group = new class extends stdClass {

      use TangibleObject;
      static $framework;

      public $name;
      public $plugins = [
        // name => $plugin
      ];
      public $actions = [];
      public $is_loaded = false;

      public function unique_action($action_name, $callback) {

        if (isset($this->actions[$action_name])) return;

        $this->actions[$action_name] = $callback;

        // Run when group plugins are all loaded, so the callback can access members or shared config
        $this->loaded(function($group, $framework) use ($callback) {
          $callback($group, $framework);
        });
      }

      public function loaded($callback) {
        if ($this->is_loaded) return $callback($this, self::$framework);
        add_action("tangible_plugin_group_loaded_{$this->name}", function($group, $framework) use ($callback) {
          $callback($group, $framework);
        }, 10, 2);
      }
    };

    $group::$framework = $framework;
    $group->name = $group_name;

    if ($framework->plugin_group_state['is_loaded']) {
      $group->is_loaded = true;
    }

    $framework->plugin_group_state['groups'][$group_name] = $group;
  }

  $plugin_name = is_array($plugin) ? $plugin['name'] : $plugin->config['name'];

  $group->plugins[$plugin_name] = $plugin;

  return $group;
};

add_action('plugins_loaded', function() use ($framework) {

  foreach ($framework->plugin_group_state['groups'] as $group) {
    do_action("tangible_plugin_group_loaded_{$group->name}", $group, $framework);
    $group->is_loaded = true;
  }

  do_action('tangible_plugin_groups_loaded', $framework);
  $framework->plugin_group_state['is_loaded'] = true;

}, 99); // After all register_plugin called
