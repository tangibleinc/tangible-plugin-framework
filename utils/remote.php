<?php

// Parse reponse from wp_remote_post()
// Used in ./license

$framework->response_body = function($response) use ($framework) {
  return json_decode(wp_remote_retrieve_body($response));
};

$framework->response_code = function($response) use ($framework) {
  return wp_remote_retrieve_response_code($response);
};
