<?php

/**
 * Performance profiler
 *
 * Uses $framework->log() which appends messages to `wp-content/log.txt`
 *
 * Example usage:
 *
 * ```php
 * $done = $framework->start_profiler();
 *
 * ..Perform action..
 *
 * $done('Action name', 1); // Log if action takes longer than 1 second
 * ```
 *
 * @return function Call this when profiled action is completed
 */

$framework->start_profiler = function() use ($framework) {

  $start_time = microtime(true);

  /**
   * End current profile, and log message
   *
   * @param string $name              Name for log, for example the plugin and function name
   * @param float $min_time_elapsed   Minimum time elapsed in seconds (optional)
   * @param bool $log_memory          Set `true` to log memory usage (optional)
   *
   * @return bool If more than minimum time elapsed for log
   */
  return function($name, $min_time_elapsed = 0, $log_memory = false) use ($framework, $start_time) {

    $time_elapsed = (float) microtime(true) - $start_time;
    $time_elapsed_rounded = number_format( $time_elapsed, 4, '.', '');

    if ($time_elapsed_rounded < $min_time_elapsed) return false;

    $message = "{$name}: {$time_elapsed_rounded}s";

    if ($log_memory) {
      $memory_usage = number_format( memory_get_usage() / (1024 * 1024), 2, '.', '');
      $message .= ", {$memory_usage} MB";
    }

    if (!defined('TANGIBLE_PROFILER') || TANGIBLE_PROFILER) {
      $framework->log($message);
    }

    return true;
  };
};

$framework->get_function_name = function($obj) {

  $anon = 'Anonymous function';
  $name = $anon;

  if (!is_callable($obj, false, $name)) return $anon;

  // is_callable fills in this variable
  return $name;
};
