<?php

$framework->string_starts_with = function($string, $start) {
  return substr( $string, 0, strlen($start) ) === $start;
};

$framework->string_ends_with = function($string, $end) {

  $length = strlen($end);
  if ($length == 0) return true;

  return substr($string, -$length) === $end;
};

$framework->string_includes = function($string, $search) {
  return strpos($string, $search) !== false;
};
