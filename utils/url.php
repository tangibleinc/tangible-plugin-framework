<?php

$framework->get_current_url = function() {

  $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === false ? 'http' : 'https';
  $host     = $_SERVER['HTTP_HOST'];
  $script   = $_SERVER['SCRIPT_NAME'];
  $params   = $_SERVER['QUERY_STRING'];
  $url      = $protocol . '://' . $host . $script . '?' . $params;

  return $url;
};
