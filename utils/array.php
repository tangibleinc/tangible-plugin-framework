<?php

/**
 * Check if an array is associative, with key => value, distinct from integer-indexed array
 *
 * @param $array The array to check.
 * @return boolean True if associative array, false otherwise
 */
$framework->is_associative_array = function($array) {
  return is_array($array) && !empty($array) &&
    array_keys($array) !== range(0, count($array) - 1)
  ;
};

/*
 * Inserts a new key/value before the key in the array.
 *
 * @param $key The key to insert before.
 * @param $array An array to insert in to.
 * @param $new_key The key to insert.
 * @param $new_value An value to insert.
 *
 * @return The new array if the key exists, false otherwise.
 */
$framework->array_insert_before = function($key, array &$array, $new_key, $new_value) {
  if (array_key_exists($key, $array)) {
    $new = array();
    foreach ($array as $k => $value) {
      if ($k === $key) {
        $new[$new_key] = $new_value;
      }
      $new[$k] = $value;
    }
    return $new;
  }
  return false;
};

/*
 * Inserts a new key/value after the key in the array.
 *
 * @param $key The key to insert after.
 * @param $array An array to insert in to.
 * @param $new_key The key to insert.
 * @param $new_value An value to insert.
 *
 * @return The new array if the key exists, false otherwise.
 */
$framework->array_insert_after = function($key, array &$array, $new_key, $new_value) {
  if (array_key_exists($key, $array)) {
    $new = array();
    foreach ($array as $k => $value) {
      $new[$k] = $value;
      if ($k === $key) {
        $new[$new_key] = $new_value;
      }
    }
    return $new;
  }
  return false;
};
