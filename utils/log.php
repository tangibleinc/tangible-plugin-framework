<?php

// Logging

// Keep object references to detect recursion
$framework::$tmp->seen = [];

$framework->see = function() use ($framework) {

  $trace = debug_backtrace();
  $caller = $trace[1];
  $file = str_replace(ABSPATH, '', $caller['file']);

  echo "<br><b>{$file}</b> in <b>{$caller['line']}</b><br>";

  $args = func_get_args();

  $framework::$tmp->seen = [];

  ?><pre><code><?php
    foreach ($args as $arg) {
      print_r($this->format_code($arg));
      echo "\n";
    }
  ?></code></pre><?php

  $framework::$tmp->seen = [];

  if ( isset( $args[0] ) ) return $args[0];
};

$framework->format_function_location = function($f) {
  return ' in '.str_replace(ABSPATH, '', $f->getFileName()).' on line '.$f->getStartLine();
};

$framework->format_function_name = function($obj) use ($framework) {

  $name = '';
  if (!is_object($obj) && !is_callable($obj, false, $name)) return;
  try {
    if (is_array($obj)) {
      $f = new ReflectionMethod($obj[0], $obj[1]);
      return $f->class.'::'.$f->name . $framework->format_function_location($f);
    }
    if (is_object($obj)) {
      if ($obj instanceof Closure) {
        $f = new ReflectionFunction($obj);
        return 'Anonymous function' . $framework->format_function_location($f);
      }
      $f = new ReflectionClass($obj);
      return ($f->isAnonymous() ? (
        isset( $obj->name ) ? 'Object ' . $obj->name
          : 'Anonymous class'
      ) : 'Class '.$f->name) . $framework->format_function_location($f);
    }
    $f = new ReflectionFunction($obj);
    return 'Function '.$f->name . $framework->format_function_location($f);
  } catch (\Throwable $th) {}

  return $name;
};

$framework->format_code = function($obj) use ($framework) {

  $find = ['<', '>'];
  $replace = ['&lt;','&gt;'];

  if (is_string($obj)) return str_replace($find, $replace, $obj);
  if (is_bool($obj)) return ( $obj ? 'TRUE' : 'FALSE' );
  if (is_null($obj) ) return "NULL";
  if (is_numeric($obj) ) return $obj;
  if (!is_array($obj) && !is_object($obj)) return $obj;

  $is_class_instance = is_object($obj) && !($obj instanceof Closure);

  $name = $framework->format_function_name($obj);
  if (!empty($name) && !$is_class_instance) return $name;

  $newObj = [];
  if ($is_class_instance && !empty($name)) $newObj['__instance__'] = $name;

  foreach ($obj as $key => $value) {
    $seen = false;
    if (is_object($value)) {
      foreach ($framework::$tmp->seen as $seen_value) {
        if ($seen_value!==$value) continue;

        $name = $framework->format_function_name($value);
        if (!empty($name)) $newObj[$key] = $name;
        else $newObj[$key] = '*RECURSION*';

        $seen = true;
        break;
      }
      if ($seen) continue;
      $framework::$tmp->seen []= $value;
    }
    $newObj[$key] = $this->format_code($value);
  }

  return $newObj;
};

$framework->trace = function($depth = 0) {

  $stack = (new Exception())->getTrace();
  array_shift($stack); // Remove self

  ?><br><?php
  foreach ($stack as $index => $caller) {

    if ($depth > 0 && $index > $depth) break;

    $next_caller = isset($stack[ $index+1 ])
      ? $stack[ $index+1 ]
      : []
    ;

    $cls = isset($next_caller['class']) ? $next_caller['class'] : '';
    if (strpos($cls, '@anonymous')!==false) $next_caller = '$this';

    $fn = isset($next_caller['function']) ? $next_caller['function'] : '';

    $full_fn = ($fn==='__call' && isset($next_caller['args']) && isset($next_caller['args'][0]))
      ? $next_caller['args'][0]
      : (!empty($cls) ? "$cls::" : '').$fn;

    $file = isset($caller['file']) ? str_replace(ABSPATH, '', $caller['file']) : 'unknown file';
    $line = isset($caller['line']) ? $caller['line'] : 'unknown';

    echo "$full_fn - <b>{$file}</b> in line <b>{$line}</b><br>";
  }
};


$framework->log_to_file = function() {

  $args = func_get_args();
  if (empty($args)) return;

  // First argument is the log file path

  $log_path = $args[0];
  $file = fopen($log_path, 'a');
  if (!$file) return;
  array_shift($args);

  ob_start();

  foreach ($args as $arg) {
    if (is_string($arg)) echo $arg;
    else print_r($arg);
    echo "\n";
  }

  fwrite($file, ob_get_clean());
  fclose($file);
};

$framework->log = function() use ($framework) {
  $args = func_get_args();
  array_unshift($args, WP_CONTENT_DIR.'/log.txt');
  return call_user_func_array([$framework, 'log_to_file'], $args);
};
