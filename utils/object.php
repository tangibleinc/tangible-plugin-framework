<?php

// State object with actions

$framework->object = function($props = []) {

  $o = new class extends stdClass {
    use TangibleObject;
    public $state = [];
  };

  foreach ($props as $key => $value) {
    $o->{$key} = $value;
  }

  return $o;
};
