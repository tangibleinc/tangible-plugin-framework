<?php

include __DIR__.'/activate.php';
include __DIR__.'/deactivate.php';
include __DIR__.'/fields.php';
include __DIR__.'/notice.php';
include __DIR__.'/settings-tab.php';

include __DIR__.'/check.php';

// Called from ../core/init.php

$framework->license_action = function($plugin) use ($framework) {

  $license_key = $framework->get_license_key($plugin);
  if (isset($_POST[$license_key])) {
    $license = $_POST[$license_key];
    $framework->set_license($plugin, $license);
  }

  // If activate/deactivate button was clicked - See ./settings-tab.php

  $prefix = $plugin['setting_prefix'];

  if (isset($_POST["{$prefix}_license_activate"])) {

    $framework->activate_license($plugin);

  } elseif(isset($_POST["{$prefix}_license_deactivate"])) {

    $framework->deactivate_license($plugin);
  }

};
