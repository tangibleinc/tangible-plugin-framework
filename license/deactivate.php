<?php

$framework->deactivate_license = function($config) use ($framework) {

  $api = $framework->get_api($config);
  $prefix = $config['setting_prefix'];
  $nonce_key = "{$prefix}_nonce";

  if (!check_admin_referer($nonce_key, $nonce_key)) return;

  $item_id = $config['item_id'];

  $license_page_url = $framework->get_license_page_url($config);

  // EDD store API request

  $response = $framework->store_action($config, 'deactivate_license', [
    'license' => $framework->get_license($config)
  ]);;

  // Response

  if (is_wp_error($response) || 200!==$framework->response_code($response)) {

    if ( is_wp_error( $response ) ) {
      $message = $response->get_error_message();
    } else {
      $message = __( 'An error occurred, please try again.' );
    }

    $api->log('Error: deactivate_license', $framework->response_body($response));

    // Checked in ./notice
    $activation_status_key = "{$prefix}_activation";

    wp_redirect(add_query_arg([
      $activation_status_key => 'false',
      'message' => urlencode($message)
    ], $license_page_url));
    exit;
  }

  $license_data = $framework->response_body($response);

  // $license_data->license will be either "deactivated" or "failed"
  //if ($license_data->license==='deactivated') {
  $framework->reset_license_status($config);
  //}

  $api->log('deactivate_license', (array) $license_data);

  wp_redirect($license_page_url);
  exit();

};
