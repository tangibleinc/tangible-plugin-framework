<?php

$framework->register_license_field = function($plugin) use ($framework) {

  $settings_key = $framework->get_settings_key($plugin);
  $license_key = $framework->get_license_key($plugin);

  register_setting($license_key, $license_key, function ($new) use ($framework, $plugin) {

    $old = $framework->get_license($plugin);

    // Reset status if new license
    if ($old && $old != $new ) {
      $framework->reset_license_status($plugin);
    }

    return $new;
  });
};

$framework->get_license_key = function($plugin) use ($framework) {
  return "{$plugin['setting_prefix']}_license_key";
};

$framework->get_license = function($plugin) use ($framework) {

  $key = $framework->get_license_key($plugin);
  $license = $framework->get_setting($plugin, $key);

  if (empty($license)) return '';
  return trim($license);
};

$framework->set_license = function($plugin, $license) use ($framework) {
  $key = $framework->get_license_key($plugin);
  return $framework->update_setting($plugin, $key, $license);
};

$framework->get_license_page_url = function($plugin) use ($framework) {
  return $framework->get_settings_page_url($plugin, 'license');
};

$framework->has_valid_license = function($plugin) use ($framework) {
  $license = $framework->get_license($plugin);
  $license_status  = $framework->get_license_status($plugin);
  return !empty($license) && $license_status==='valid';
};

// License status for internal use

$framework->get_license_status_key = function($plugin) use ($framework) {
  return "{$plugin['setting_prefix']}_license_status";
};

$framework->get_license_status = function($plugin) use ($framework) {
  $key = $framework->get_license_status_key($plugin);
  return $framework->get_setting($plugin, $key);
};

$framework->set_license_status = function($plugin, $status) use ($framework) {
  $key = $framework->get_license_status_key($plugin);
  return $framework->update_setting($plugin, $key, $status);
};

$framework->reset_license_status = function($plugin) use ($framework) {
  $key = $framework->get_license_status_key($plugin);
  return $framework->delete_setting($plugin, $key);
};
