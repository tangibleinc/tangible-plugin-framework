<?php

$framework->render_license_tab = function($plugin) use ($framework) {

  $prefix = $plugin['setting_prefix'];

  $license = $framework->get_license($plugin);
  $license_key = $framework->get_license_key($plugin);
  $license_status  = $framework->get_license_status($plugin);

  $has_valid_license = !empty($license) && $license_status==='valid';

  $deactivate_key = "{$prefix}_license_deactivate";
  $activate_key = "{$prefix}_license_activate";

  settings_fields($license_key);

  ?>
  <div class="tangible-plugin-settings-tab-license">

    <h3>
      <?php _e('License Key'); ?>
      <div class="license-status">&mdash;&nbsp;<?php

      if ($has_valid_license) {
        ?><span class="success">Active</span><?php
      } else {
        ?><span class="error">Not active</span><?php
      }

    ?></div></h3>

    <div class="setting-row">
      <input type="password" class="regular-text"
        id="<?php echo $license_key; ?>" name="<?php echo $license_key; ?>"
        value="<?php echo esc_attr_e( $license ); ?>"
        autocomplete="off"
        <?php
        // Can't prevent "Save password?" prompt on Chrome
        // onblur='this.type="password"' onfocus='this.type="text"'
        ?>
      />
    </div>

    <?php
    if ($has_valid_license) {
      ?><input type="submit" class="button-secondary"
        name="<?php echo $deactivate_key; ?>" value="<?php _e('Deactivate License');
      ?>"/><?php
    } else {
      ?><input type="submit" class="button-primary"
        name="<?php echo $activate_key; ?>" value="<?php
          _e(empty($license) ? 'Save License Key' : 'Activate License');
      ?>"/><?php
    }

    //submit_button();

  ?></div><?php

};
