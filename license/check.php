<?php

/**
 * Manually call the store and check if a plugin's license key is valid
 */

$framework->is_license_valid = function($plugin) use ($framework) {

  $config = $framework->get_config( $plugin );

  $response = $framework->store_action($config, 'check_license', [
    'license' => $framework->get_license($config)
  ]);

  if (is_wp_error($response)) return false;

  $license_data = $framework->response_body($response);

  if ($license_data->license == 'valid') {
    return true;
  }

  return false;
};
