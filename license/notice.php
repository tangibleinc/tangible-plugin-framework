<?php

$framework->register_license_notice = function($plugin) use ($framework) {

  $action = $framework->is_multisite($plugin)
    ? 'network_admin_notices'
    : 'admin_notices';

  add_action($action, function() use ($plugin) {

    $prefix = $plugin['setting_prefix'];

    // From ./activate or ./deactivate
    $activation_status_key = "{$prefix}_activation";

    // Check activation messages

    if (!isset($_GET[$activation_status_key]) || empty($_GET['message'])) return;

    $message = urldecode(@$_GET['message']);

    switch( $_GET[$activation_status_key] ) {
      case 'false':
        ?><div class="error"><p><?php echo $message; ?></p></div><?php
        break;
      case 'true':
      default:
        ?><div class="success"><p><?php echo $message; ?></p></div><?php
        break;
    }
  });

};
