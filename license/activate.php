<?php

$framework->activate_license = function($config) use ($framework) {

  $api = $framework->get_api($config);
  $prefix = $config['setting_prefix'];
  $nonce_key = "{$prefix}_nonce";

  if (!check_admin_referer($nonce_key, $nonce_key)) return;

  $item_id = $config['item_id'];
  $plugin_title = $config['title'];

  $license_page_url = $framework->get_license_page_url($config);

  // EDD store API request

  $response = $framework->store_action($config, 'activate_license', [
    'license' => $framework->get_license($config)
  ]);

  // Response

  if (is_wp_error($response) || 200!==$framework->response_code($response)) {

    if ( is_wp_error( $response ) ) {
      $message = $response->get_error_message();
    } else {
      $message = __( 'An error occurred, please try again.' );
    }

  } else {

    $license_data = $framework->response_body($response);

    if ( empty($license_data) ) {
      $license_data = new class {
        public $success = false;
        public $error   = 'unknown';
      };
    }

    if ( false === $license_data->success ) {
      switch( $license_data->error ) {
        case 'expired' :
          $message = sprintf(
            __( 'Your license key expired on %s.' ),
            date_i18n( get_option( 'date_format' ), strtotime( $license_data->expires, current_time( 'timestamp' ) ) )
          );
          break;
        case 'revoked' :
          $message = __( 'Your license key has been disabled.' );
          break;
        case 'missing' :
          $message = __( 'Invalid license.' );
          break;
        case 'invalid' :
        case 'site_inactive' :
          $message = __( 'Your license is not active for this URL.' );
          break;
        case 'item_id_mismatch' :
          $message = sprintf( __( 'This appears to be an invalid license key for %s.' ), $plugin_title );
          break;
        case 'no_activations_left':
          $message = __( 'Your license key has reached its activation limit.' );
          break;
        case 'invalid_item_id':
          $message = sprintf( __( 'The license key is not valid for product ID %s.' ), @$config['item_id'] );
          break;
        default:
          $message = __( 'An error occurred, please try again.' );
      }
    }
  }

  // Checked in ./notice
  $activation_status_key = "{$prefix}_activation";

  // If there's a failure message

  if (!empty($message)) {

    wp_redirect(add_query_arg([
      $activation_status_key => 'false',
      'message' => urlencode($message)
    ], $license_page_url));

    $api->log('Error: activate_license', $framework->response_body($response));

    exit;
  }

  $framework->set_license_status($config, $license_data->license);

  $api->log('activate_license', (array) $license_data);

  wp_redirect(add_query_arg([
    $activation_status_key => 'true',
    'message' => urlencode('License activation was successful.')
  ], $license_page_url));

  exit();
};
